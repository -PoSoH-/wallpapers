package com.gulievferdihotmail.wellpaperslife.Models;

/**
 * Created by ADMIN on 04.03.2016.
 */
public class ModelCategories {
    String objectId;
    String name;
    String imageURL;
    boolean pay;
    String productId;

    public void init(String objectId, String name, String imageURL, boolean pay, String productId){
        this.objectId = objectId;
        this.name = name;
        this.imageURL = imageURL;
        this.pay = pay;
        this.productId = productId;
    }

    public String getObjectId(){return this.objectId;}
    public String getNameCategory(){return this.name;}
    public String getImageURL() {return this.imageURL;}

    public boolean isPay() {
        return pay;
    }

    public String getProductId() {
        return productId;
    }
}
