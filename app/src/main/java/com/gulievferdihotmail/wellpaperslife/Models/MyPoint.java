package com.gulievferdihotmail.wellpaperslife.Models;

import android.graphics.Paint;

/**
 * Created by ADMIN on 01.03.2016.
 */
public class MyPoint {
    String text;
    private int x;
    private int y;
    private Paint paint;

    public MyPoint(String text, int x, int y) {
        this.text = text;
        this.x = x;
        this.y = y;
    }

    public MyPoint(String text, int x, int y, Paint paint) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.paint = new Paint(paint);
    }

    public Paint getPaint() {return this.paint;}
    public int getX() {return x;}
    public int getY() {return y;}
}
