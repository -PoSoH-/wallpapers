package com.gulievferdihotmail.wellpaperslife.Models;

/**
 * Created by ADMIN on 04.03.2016.
 */
public class ModelWallpaper {
    String _ObjectId;
    String _Name;
    String _ImageURL;
    String _Categories;
    String _Downloads;
    String _Favorites;
    long _Data;
    boolean _Pay;
    String _Tags;
    boolean _Live;
    String _ProductId;
    Double _Price;

    public void init(String objectId
            , String name
            , String imageURL
            , String categories
            , String downloads
            , String favorites
            , long data
            , boolean pay
            , String tags
            , boolean live
            , String productId
            , Double _Price){
        this._ObjectId = objectId;
        this._Name = name;
        this._ImageURL = imageURL;
        this._Categories = categories;
        this._Downloads = downloads;
        this._Favorites = favorites;
        this._Data = data;
        this._Pay = pay;
        this._Tags = tags;
        this._Live = live;
        this._ProductId = productId;
        this._Price = _Price;
    }

    public void setLikes(int count){this._Favorites = String.valueOf(count);}
    public void setDownloads(int downloads) { _Downloads = String.valueOf(downloads); }

    public String getObjectId(){return this._ObjectId;}
    public String getNameCategory(){return this._Name;}
    public String getImageURL() {return this._ImageURL;}
    public String getDownloads(){return this._Downloads;}
    public String getFavorites(){return this._Favorites;}
    public long getData(){return this._Data;}
    public boolean isPay() {return this._Pay;}
    public String getTags(){return this._Tags;}
    public boolean getLive(){return this._Live;}

    public Double get_Price() {
        return _Price;
    }

    public String get_ProductId() {
        return _ProductId;
    }
}
