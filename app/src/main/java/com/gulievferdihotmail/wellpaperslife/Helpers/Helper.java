package com.gulievferdihotmail.wellpaperslife.Helpers;

import android.content.Context;
import android.util.Log;

import com.backendless.media.Stream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;

/**
 * Created by Sergiy Polishuk on 01.03.2016.
 */
public class Helper {

    public static final String FRAGMENT_ACCOUNT_TYPE = "FRAGMENT_ACCOUNT_TYPE";
    public static final String TYPE_REGISTRATION = "REGISTRATION";
    public static final String TYPE_LOGIN = "LOGIN";
    public static final String TYPE_FORGOT_PASSWORD = "FORGOT";
    public static final String TYPE_CHANGE_PASSWORD = "CHANGE";
    public static final String TYPE_MAIN = "MAIN";

    public static final String WALLPAPER_STATIC = "/static";
    public static final String WALLPAPER_DYNAMIC = "/dynamic";
    public static final String WALLPAPER_LIKED = "/list_liked/list.txt";

    public static final boolean LOG = true;

    public static final String EMAIL_VALIDATION_PATTERN = "^[-\\w.]+@([A-z0-9]+\\.)+[A-z]{2,}$";
    public static final String NOT_NUMBER_PATTERN = "[^0-9]";
    public static final int MIN_NUMBER_PASSWORD = 6;
    public static final int MIN_NUMBER_FIRST_NAME = 5;
    public static final int MIN_NUMBER_LAST_NAME = 5;
    public static final int MIN_NUMBER_CITY_NAME = 4;

    public static final long SECOND = 1000;
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
    public static final long WEEK = DAY * 7;

    public static void logOut(String place, String value){
        if(LOG) {
            Log.d("WALLPAPER"
                    , ".." + place + ".." + value + "..");
        }
    }

    public static boolean isNewAddList(long timeCreatedMilis){
        Calendar c = Calendar.getInstance();
        long seconds = c.get(Calendar.MILLISECOND);
        long value = seconds - WEEK;
        if(value>timeCreatedMilis)
            return true;
        return false;
    }

    public static String parseTags(String inString){
        return "Tags: " + inString;
    }

    public static void saveFileToDisk(final Context context, Byte stream, String name){
        File file = new File(context.getFilesDir() + WALLPAPER_STATIC + "/", name);
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            outputStream.write(stream);
            outputStream.close();
            logOut("SAVE FILE", "COMPLETED");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFileLikedList(final Context context, String objectIdLike){
        context.getFilesDir();

    }

    public interface onListenerFinalSave{
        void onSuccess();
        void onError();
    }

}
