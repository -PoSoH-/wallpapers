package com.gulievferdihotmail.wellpaperslife.Helpers;

import android.app.Activity;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class IAPHelper {

    public static final String IN_APP_BILLING_ACTION = "com.android.vending.billing.InAppBillingService.BIND";
    public static final String VENDING_PACKAGE = "com.android.vending";
    public static final int BUY_REQUEST_CODE = 1001;
    public static final int API_VERSION = 3;
    public static final String INAPP_TYPE = "inapp";
    public static final String SUBS_SKU = "android.test.purchased";
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String INAPP_PURCHASE_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String PRODUCT_ID = "productId";
    public static final String TAG = "Wallpaper";
    private Activity activity;
    private IInAppBillingService mService;
    private InAppListener listener;
    private String skuToBuy;
    private boolean purchaseInProgress;

    public IAPHelper(Activity activity) {
        this.activity = activity;
    }

    public interface InAppListener {
        void bought(String skuToBuy);
    }

    public void setListener(InAppListener listener) {
        this.listener = listener;
    }

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "InAppHelper -> onServiceDisconnected");
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "InAppHelper -> onServiceConnected -> PackageName: " + activity.getPackageName());
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };

    public void getSkuDetails() {
        ArrayList<String> skuList = new ArrayList<>();
        skuList.add(SUBS_SKU);
        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

        try {
            Bundle skuDetails = mService.getSkuDetails(API_VERSION, activity.getPackageName(), INAPP_TYPE, querySkus);
            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String> responseList
                        = skuDetails.getStringArrayList("DETAILS_LIST");
                for (String thisResponse : responseList) {
                    JSONObject object;

                    object = new JSONObject(thisResponse);

                    String sku = object.getString("productId");
                    String price = object.getString("price");
                }
            }
        } catch (RemoteException | JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkIfBoughtOrBuy(final String skuToBuy) {
        // Check if product already bought
        if (mService == null) return;
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                boolean bought = false;
                try {
                    Bundle ownedItems = mService.getPurchases(API_VERSION, activity.getPackageName(), INAPP_TYPE, null);
                    int response = ownedItems.getInt(RESPONSE_CODE);

                    Log.d(TAG, "InAppHelper -> onServiceConnected -> getPurchases response: " + response);

                    if (response == 0) {
                        ArrayList<String> ownedSkus = ownedItems.getStringArrayList(INAPP_PURCHASE_ITEM_LIST);
                        if (ownedSkus != null && ownedSkus.contains(skuToBuy)) bought = true;
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                return bought;
            }

            @Override
            protected void onPostExecute(Boolean bought) {
                if (bought) listener.bought(skuToBuy);
                else buy(skuToBuy);
            }
        }.execute();
    }

    private void buy(String skuToBuy) {
        if (purchaseInProgress) {
            Toast.makeText(activity, "Buy in progress", Toast.LENGTH_LONG).show();
            return;
        }
        purchaseInProgress = true;
        this.skuToBuy = skuToBuy;
        Log.d(TAG, "InAppHelper -> subscribe");

        try {
            Bundle purchaseBundle = mService.getBuyIntent(API_VERSION, activity.getPackageName(),
                    skuToBuy, INAPP_TYPE, null);

            PendingIntent pendingIntent = purchaseBundle.getParcelable("BUY_INTENT");
            if (pendingIntent != null){
//                if (subscriptionsBundle.getInt(RESPONSE_CODE) == BILLING_RESPONSE_RESULT_OK) {
                // Start purchase flow (this brings up the Google Play UI).
                // Result will be delivered through onActivityResult().

                activity.startIntentSenderForResult(pendingIntent.getIntentSender(),
                        BUY_REQUEST_CODE, new Intent(), 0, 0, 0);
            }
        } catch (IntentSender.SendIntentException | RemoteException e) {
            Log.d(TAG, "InAppHelper -> subscribe error: " + e.getMessage());
            e.printStackTrace();
            purchaseInProgress = false;
        }
    }

    public void onCreate() {
        Intent serviceIntent = new Intent(IN_APP_BILLING_ACTION);
        serviceIntent.setPackage(VENDING_PACKAGE);
        activity.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "InAppHelper -> onActivityResult -> requestCode: " + requestCode + ", resultCode: " + resultCode);

        if (requestCode == BUY_REQUEST_CODE) {
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            if (resultCode == Activity.RESULT_OK && purchaseData != null) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString(PRODUCT_ID);
                    if (skuToBuy.equals(sku)) listener.bought(skuToBuy);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            purchaseInProgress = false;
        }
    }

    public void onDestroy() {
        if (mService != null) activity.unbindService(mServiceConn);
        activity = null;
    }
}
