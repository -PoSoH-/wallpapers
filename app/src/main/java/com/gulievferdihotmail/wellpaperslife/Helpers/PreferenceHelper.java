package com.gulievferdihotmail.wellpaperslife.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by android_2 on 23.12.2015.
 */
public class PreferenceHelper {

    public static final String PREFERENCES = "com.gulievferdihotmail.wellpaperslife.Helpers.helpers.preference.PREFERENCES";

    public static final String SAVE_NO_READ_MESSAGE = "SAVE_NO_READ_MESSAGE";
    public static final String SAVE_CONTACTS = "SAVE_CONTACTS";
    public static final String ID_OPENED_CHAT = "ID_OPENED_CHAT";
    public static final String LIKE_LIST_KEY = "LIKE_LIST_KEY";
    public static final String PURCHASE_LIST_KEY = "PURCHASE_LIST_KEY";
    public static final String DOWNLOADS_LIST_KEY = "DOWNLOADS_LIST_KEY";

    public static SharedPreferences getPreferences(final Context context){
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }



    public static String getStringNoReadMessage(Context context){
        SharedPreferences preferences = getPreferences(context);
        return preferences.getString(SAVE_NO_READ_MESSAGE, null);
    }

    public static void setStringValuePreferences(Context context, String key, String value){
        SharedPreferences preferences = getPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(key, value);
        boolean success = edit.commit();
        if(success) Log.i("SAVE SHARED PREFERENCE", "   SAVE "+ value +" IS OK!..");
        else Log.i("SAVE SHARED PREFERENCE", "SAVE "+ value +" ERROR!..");
    }

    public static void deletePreferences(Context context, String key){
        SharedPreferences preferences = getPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.remove(key);
        boolean success = edit.commit();
        if(success) Log.i("SAVE SHAREDPREFERENCE", " SUCCESS DELETE " + key);
        else Log.i("SAVE SHAREDPREFERENCE", " ERROR DELETE " + key);
    }

    public static String getStringValuePreferences(Context context, String key, String defValue){
        SharedPreferences preferences = getPreferences(context);
        return preferences.getString(key, defValue);
    }


    //==============================================

    private static final String LIVE_WALLPAPER_ABSOLUTE_PATH = "LIVE_WALLPAPER_ABSOLUTE_PATH";

    public static void setStringAbsolutePath(Context context, String value){
        SharedPreferences preferences = getPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(LIVE_WALLPAPER_ABSOLUTE_PATH, value);
        boolean success = edit.commit();
        if(success)
            Log.d("work", "Preferences : save is ok" + value);
        else
            Log.d("work", "Preferences : save is error");
    }

    public static String getStringAbsolutePath(Context context){
        SharedPreferences preferences = getPreferences(context);
        return preferences.getString(LIVE_WALLPAPER_ABSOLUTE_PATH, null);
    }

    //==============================================

    public static void writeStringDataList(Context context, final String key, ArrayList<String> setObjects){
        SharedPreferences preferences = getPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        JSONArray array = new JSONArray(setObjects);
        edit.putString(key, array.toString());
        boolean success = edit.commit();
        if(success)
            Log.d("work", "Preferences : save is ok");
        else
            Log.d("work", "Preferences : save is error");
    }

    public static ArrayList<String> getStringDataList(Context context, final String key){
        SharedPreferences preferences = getPreferences(context);
        String data = preferences.getString(key, null);
        ArrayList<String> values = new ArrayList<>();
        if(data != null){
            JSONArray arrayList;
            try {
                arrayList = new JSONArray(data);
                for(int p=0, l=arrayList.length(); p<l; p++){
                    values.add(arrayList.getString(p));
                }
                return values;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return values;
    }

    public static void addPurchasedId(String id, Context context) {
        ArrayList<String> purchasedIds = PreferenceHelper.getStringDataList(context,
                PreferenceHelper.PURCHASE_LIST_KEY);
        if (purchasedIds == null) purchasedIds = new ArrayList<>();
        purchasedIds.add(id);
        PreferenceHelper.writeStringDataList(context, PreferenceHelper.PURCHASE_LIST_KEY, purchasedIds);
    }
}
