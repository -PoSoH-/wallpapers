package com.gulievferdihotmail.wellpaperslife.Activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.R;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */

public class Base extends AppCompatActivity {

    private View viewToolbar;

    private View viewTextInfo;
    private View viewText;
    private View viewSearch;

    private TextView txtCenterUp;
    private TextView txtCenterDown;

    private ImageView btnBack;
    private ImageView btnMenu;
    private ImageView btnMenuPoint;
    private ImageView btnSearch;
    private ImageView btnEndSearch;
//    protected ImageView btnClose;
//    private boolean viewDialog = true;

    RelativeLayout fullLayout;

    private LinearLayout layBlockData = null;
    private LinearLayout viewDownBtnBlock = null;

    private SharedPreferences preferences = null;

    public static void blockOrientationChange(Activity activity){
        if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public static void restoreOrientationChange(Activity activity){
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

//    public void setPreferencesValue(final String keyData, String valueData){
//        preferences = getPreferences(MODE_PRIVATE);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString(keyData, valueData);
//        editor.commit();
//    }
//
//    public void setPreferencesName(final String keyData, String valueData){
//        preferences = getPreferences(MODE_PRIVATE);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString(keyData, valueData);
//        boolean info = editor.commit();
//        if(info){
//            Log.i("PREFERENCE ", " OK!");
//        }else{
//            Log.e("PREFERENCE ", " SAVED IS NOT COMPLETE!...");
//        }
//    }
//
//    public String getPreferencesValue(final String keyData){
//        preferences = getPreferences(MODE_PRIVATE);
//        return preferences.getString(keyData, "");
//    }

    @Override
    public void setContentView(int layoutResID) {
        setContentView(getLayoutInflater().inflate(layoutResID, null));
    }

    @Override
    public void setContentView(View view) {
        fullLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.a_base_container, null);
        RelativeLayout contentLayout = (RelativeLayout) fullLayout.findViewById(R.id.a_base_container);
        contentLayout.addView(view);
        initializeControls(fullLayout);
        super.setContentView(fullLayout);
    }

    public RelativeLayout getRelativeLayout(){return fullLayout;}

    private void initializeControls(View view){
        viewToolbar = view.findViewById(R.id.toolbar_all_view);

        btnBack = (ImageView) view.findViewById(R.id.toolbar_btn_back);
        btnMenu = (ImageView) view.findViewById(R.id.toolbar_btn_menu);
        btnMenuPoint = (ImageView) view.findViewById(R.id.toolbar_btn_menu_point);
        btnSearch = (ImageView) view.findViewById(R.id.toolbar_btn_search);
        btnEndSearch = (ImageView) view.findViewById(R.id.toolbar_btn_end_search);

        viewToolbar = view.findViewById(R.id.a_toolbar);

        viewTextInfo = view.findViewById(R.id.toolbar_center_info_all);
        viewSearch = view.findViewById(R.id.toolbar_view_center_search);
        viewText = view.findViewById(R.id.toolbar_view_center_text);

        txtCenterUp = (TextView) view.findViewById(R.id.toolbar_txt_name_up);
        txtCenterDown = (TextView) view.findViewById(R.id.toolbar_txt_name_down);
//
//        btnBack.setOnClickListener(new_icon View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//        btnClose.setOnClickListener(new_icon View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//        setDefaultBackButtonListener();
//        closeButton.setOnClickListener(new_icon View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//        myProfileButton.setOnClickListener(new_icon View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new_icon Intent(BaseActivity.this, UserProfileActivity.class));
//                overridePendingTransition(R.anim.activity_start_enter_bottom, R.anim.activity_start_exit_bottom);
//            }
//        });

//        setSupportActionBar(toolbar);

        try {
            String label = getString(getPackageManager().getActivityInfo(getComponentName(), 0).
                    labelRes);
            if (!TextUtils.isEmpty(label)){
//                txtViewToolBar.setVisibility(View.VISIBLE);
//                txtViewToolBar.setText(label);
            }
        } catch (Exception ignored) {}

    }

    public void setUpBackButtonListener(View.OnClickListener listener){
//        btnBack.setOnClickListener(listener);
    }

//    public void setDefaultBackButtonListener(){
//        btnBack.setOnClickListener(backButtonListener);
//    }
//

    public void showToolbar(){
        viewToolbar.setVisibility(View.VISIBLE);
    }

    public void hideToolbar(){
        viewToolbar.setVisibility(View.GONE);
    }

    public void showBackButton(){
        btnBack.setVisibility(View.VISIBLE);
    }

    public void hideBackButton(){
        btnBack.setVisibility(View.GONE);
    }

    public void showMenuLeftButton(){
        btnMenu.setVisibility(View.VISIBLE);
    }
    public void hideMenuLeftButton(){
        btnMenu.setVisibility(View.GONE);
    }
    public void showMenuRightButton(){
        btnMenuPoint.setVisibility(View.VISIBLE);
    }
    public void showCenterInfoView(){
        viewTextInfo.setVisibility(View.VISIBLE);
    }

    public void setToolbarLabel(String text){
        txtCenterUp.setText(text);
    }
//
//    public void showChatSettingButton(){
//        btnChatSettings.setVisibility(View.VISIBLE);
//    }
//
//    public void hideChatSettingButton(){
//        btnChatSettings.setVisibility(View.GONE);
//    }
//
//    public void showBackButtonBlue(){
//        btnBack.setImageDrawable(getResources().getDrawable(R.drawable.btn_registration_form));
//        btnBack.setVisibility(View.VISIBLE);
//    }
//
//    public void showPlusButton(){
//        btnPlus.setVisibility(View.VISIBLE);
//    }
//
//    public void hidePlusButton(){
//        btnPlus.setVisibility(View.GONE);
//    }
//
//    public void showCloseButton(){btnClose.setVisibility(View.VISIBLE);}
//    public void hideCloseButton(){btnClose.setVisibility(View.GONE);}
//
//    public void showSaveButton(){txtSave.setVisibility(View.VISIBLE);}
//    public void hideSaveButton(){txtSave.setVisibility(View.GONE);}
//
//    public void showEditButton(){txtEdit.setVisibility(View.VISIBLE);}
//    public void hideEditButton(){txtEdit.setVisibility(View.GONE);}
//
//    public void showNexButton(){txtNext.setVisibility(View.VISIBLE);}
//    public void hideNextButton(){txtNext.setVisibility(View.GONE);}
//
//    public void showCreateButton(){txtCreate.setVisibility(View.VISIBLE);}
//    public void hideCreateButton(){txtCreate.setVisibility(View.GONE);}
//
//    public void showDoneButton(){txtDone.setVisibility(View.VISIBLE);}
//    public void hideDoneButton(){txtDone.setVisibility(View.GONE);}
//
//    public void showCloseButtonBlue(){
//        btnPlus.setImageDrawable(getResources().getDrawable(R.drawable.btn_registration_form));
//        btnPlus.setVisibility(View.VISIBLE);
//    }
//
//    public void setLabel(int resId){
//        setLabel(getString(resId));
//    }
//
//    public void setLabel(String label){
//        txtViewToolBar.setText(label);
//    }
//
//    @SuppressLint("ResourceAsColor")
//    public void setGreenToolbar() {
//        setStatusBarColor(R.color.default_green);
////        toolbar.setBackgroundColor(getResources().getColor(R.color.default_green));
//    }
//
//    @SuppressLint("ResourceAsColor")
//    public void setBlueToolbar(){
//        setStatusBarColor(R.color.white);
////        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
//    }
//
//    public void setStatusBarColor(int colorResId){
//        if (Build.VERSION.SDK_INT >= 21)
//            getWindow().setStatusBarColor(getResources().getColor(colorResId));
//    }
//
    public void setFragment(Fragment fragment, int layoutId){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        if (fragmentManager.findFragmentByTag(tag) == null)
            fragmentManager.beginTransaction().replace(layoutId, fragment, tag).commit();
    }

    public static void setFragmentWithScaleAnimation(FragmentManager fragmentManager
            , Fragment fragment
            , int layoutResIs
            , boolean addToBackStack){
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.zoom_enter_animation
                , R.anim.zoom_exit_animation);
        fragmentTransaction.replace(layoutResIs, fragment, tag);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
//
//    public Fragment getFragment(Class cl){
//        return getSupportFragmentManager().findFragmentByTag(cl.getSimpleName());
//    }
//
//    public static BaseActivity get(Activity activity){
//        return (BaseActivity) activity;
//    }
//
//    public void setCloseButtonBackground(int resId){
//        btnPlus.setBackgroundResource(resId);
//    }
//
    @Override
    public void onBackPressed() {
        onBackPressedScaleAnimation();
//        if (btnBack.getVisibility() == View.VISIBLE) onBackPressedScaleAnimation();
//        else if (btnClose.getVisibility() == View.VISIBLE){
//            super.onBackPressed();
//            overridePendingTransition(R.anim.zoom_enter_animation, R.anim.zoom_exit_animation);
//        } else super.onBackPressed();
    }

    public void onBackPressedScaleAnimation(){
        super.onBackPressed();
        overridePendingTransition(R.anim.zoom_enter_animation, R.anim.zoom_exit_animation);
    }

//    public void onBackPressedWithAnimation(){
//        super.onBackPressed();
//        overridePendingTransition(R.anim.activity_start_enter_left_side, R.anim.activity_start_exit_left_side);
//    }
//
//    public static void setRightAnimation(Activity activity){
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side,
//                R.anim.activity_start_exit_right_side);
//    }
//
//    public static void setBackAnimation(Activity activity){
//        activity.overridePendingTransition(R.anim.activity_start_enter_left_side,
//                R.anim.activity_start_exit_left_side);
//    }
//
//    public static void setDownToUpAnimation(Activity activity){
//        activity.overridePendingTransition(R.anim.activity_start_enter_bottom,
//                R.anim.activity_start_exit_bottom);
//    }
//
//    public boolean getViewDialog(){return viewDialog;}
//
//    public void setViewDialog(boolean data){viewDialog = data;}
//
//    public static void sendSMS(Activity activity, String message){
//        try {
//            Intent smsIntent = new_icon Intent(Intent.ACTION_VIEW);
//            smsIntent.setData(Uri.parse("sms:"));
//            smsIntent.setType("vnd.android-dir/mms-sms");
//            smsIntent.putExtra("sms_body", message);
//            activity.startActivity(smsIntent);
//        } catch (ActivityNotFoundException exc) {
//            ToastHelper.showErrorToast(activity.getApplicationContext(),
//                    activity.getString(R.string.no_app));
//        }
//    }
//
//    public TextView getSaveBt() {
//        return txtSave;
//    }
//
//    public static void showTopAnimation(Activity activity) {
//        activity.overridePendingTransition(R.anim.activity_start_enter_bottom,
//                R.anim.activity_start_exit_bottom);
//    }
//
//    public ImageView getBtnPlus() {
//        return btnPlus;
//    }
//
//    public void addDoneView() {
//        View successIndicator = getLayoutInflater().inflate(R.layout.success_indicator, null);
//        successIndicator.setLayoutParams(new_icon RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
//                RelativeLayout.LayoutParams.MATCH_PARENT));
//        ((ViewGroup)findViewById(android.R.id.content)).addView(successIndicator);
//    }
//
//    public void showDoneView(final Runnable animationFinished) {
//        final View successIndicator = findViewById(R.id.success_indicator);
//        if (successIndicator != null) {
//            successIndicator.setVisibility(View.VISIBLE);
//            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
//                    R.anim.fade_in_out_animation);
//            animation.setAnimationListener(new_icon Animation.AnimationListener() {
//                @Override
//                public void onAnimationStart(Animation animation) {
//                }
//
//                @Override
//                public void onAnimationEnd(Animation animation) {
//                    successIndicator.setVisibility(View.GONE);
//                    animationFinished.run();
//                }
//
//                @Override
//                public void onAnimationRepeat(Animation animation) {}
//            });
//            successIndicator.startAnimation(animation);
//        }
//    }
}
