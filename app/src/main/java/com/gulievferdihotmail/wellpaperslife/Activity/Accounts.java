package com.gulievferdihotmail.wellpaperslife.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.EditText;

import com.gulievferdihotmail.wellpaperslife.Fragment.ChangePassword;
import com.gulievferdihotmail.wellpaperslife.Fragment.Forgot;
import com.gulievferdihotmail.wellpaperslife.Fragment.Login;
import com.gulievferdihotmail.wellpaperslife.Fragment.Registration;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.R;
import com.gulievferdihotmail.wellpaperslife.Start;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */

public class Accounts extends Base implements Login.OnLoginListener
        , Registration.OnRegistrationListener
        , Forgot.OnForgotListener
        , ChangePassword.OnChangePasswordListener {

    public static void show(Activity activity,  final String type){
        Intent intent = new Intent(activity, Accounts.class);
        intent.putExtra(Helper.FRAGMENT_ACCOUNT_TYPE, type);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.zoom_enter_animation, R.anim.zoom_exit_animation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_account);
        hideToolbar();
        String type = getIntent().getStringExtra(Helper.FRAGMENT_ACCOUNT_TYPE);
        reloadInitializeFragment(type);
    }

    private void reloadInitializeFragment(String type){
        switch (type){
            case Helper.TYPE_REGISTRATION:
                setFragmentWithScaleAnimation(getSupportFragmentManager(), Registration.create(), R.id.a_account_container, true);
                break;
            case Helper.TYPE_LOGIN:
                setFragmentWithScaleAnimation(getSupportFragmentManager(), Login.create(), R.id.a_account_container, true);
                break;
            case Helper.TYPE_FORGOT_PASSWORD:
                setFragmentWithScaleAnimation(getSupportFragmentManager(), Forgot.create(), R.id.a_account_container, true);
                break;
            case Helper.TYPE_MAIN:
                Main.show(this);
                finish();
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Start.hideKeyboard(this);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onListenerEmptyField(EditText editText) {
        if(editText.getText() != null)
            if (editText.getText().toString().length()!=0)
                return true;
        return false;
    }

    @Override
    public boolean onListenerValidEmailAddress(EditText editText) {
        return editText.getText().toString().matches(Helper.EMAIL_VALIDATION_PATTERN);
    }

    @Override
    public boolean onListenerValidNumberPassword(EditText editText) {
        return editText.getText().length()>=Helper.MIN_NUMBER_PASSWORD;
    }

    @Override
    public void onListenerStartNextScreen(String typeNextScreen) {
        reloadInitializeFragment(typeNextScreen);
    }

    @Override
    public boolean onListenerCheckPassword(EditText password, EditText checkPassword) {
        return password.getText().toString().equals(checkPassword.getText().toString());
    }
}
