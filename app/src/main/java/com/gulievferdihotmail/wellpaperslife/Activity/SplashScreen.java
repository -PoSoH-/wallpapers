package com.gulievferdihotmail.wellpaperslife.Activity;

import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessInit;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessUsers;
import com.gulievferdihotmail.wellpaperslife.R;

public class SplashScreen extends AppCompatActivity {

    private GLSurfaceView glSurfaceView;
    private boolean userTokenEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_splash);
        String version = "v1"; //getPackageManager().get
        String APP_KEY = "E8401CEC-14F5-B2AE-FF97-7CD4B85F0900";
        String APP_ID = "3BA5BDFA-59C7-20A6-FF29-E96D5989AF00";
        LessInit.initBackEndLessService(getApplicationContext(), APP_ID, APP_KEY, version);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                Helper.logOut("SPLASH", "START WAIT");
                if(LessUsers.userToken()){
                    userTokenEmpty = true;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void params) {
                startScreen();
            }

        }.execute();
    }

    private void startScreen(){
//        if(userTokenEmpty) {
//            finish();
//            Accounts.show(SplashScreen.this, Helper.TYPE_LOGIN);
//        }else{
//            finish();
            Main.show(SplashScreen.this);
//        }
    }
}


