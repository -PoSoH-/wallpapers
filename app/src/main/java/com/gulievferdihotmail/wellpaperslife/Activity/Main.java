package com.gulievferdihotmail.wellpaperslife.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Adapter.SlideMenu;
import com.gulievferdihotmail.wellpaperslife.Fragment.BaseContent;
import com.gulievferdihotmail.wellpaperslife.Fragment.CategoriesView;
import com.gulievferdihotmail.wellpaperslife.Fragment.Information;
import com.gulievferdihotmail.wellpaperslife.Fragment.ViewContent;
import com.gulievferdihotmail.wellpaperslife.Fragment.WallpapersView;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Helpers.IAPHelper;
import com.gulievferdihotmail.wellpaperslife.Helpers.PreferenceHelper;
import com.gulievferdihotmail.wellpaperslife.Models.ModelCategories;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;
import com.gulievferdihotmail.wellpaperslife.R;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

public class Main extends AppCompatActivity implements View.OnClickListener
        , ViewContent.OnViewPagerListener
        , WallpapersView.OnWallpapersListener
        , CategoriesView.OnCategoriesListener
        , BaseContent.OnBaseViewListener, IAPHelper.InAppListener {

    public static final int FAVORITES = 0;
    public static final int PURCHASE = 1;
    public static final int SHARE_WITH_FRIENDS = 2;
    public static final int INFORMATION = 3;
    public static final String URL_TO_SHARE = "http://www.android.com/";

    public static final String BOUGHT_WALLPAPER_ACTION = "com.wallpaper.BOUGHT_WALLPAPER";
    public static final String BOUGHT_PRODUCT_ID = "BOUGHT_PRODUCT_ID";


    private View viewTextInfo;
    private View viewText;
    private View viewSearch;

    private TextView txtCenterUp;
    private TextView txtCenterDown;

    private ImageView btnBack;
    private ImageView btnMenu;
    private ImageView btnMenuPoint;
    private ImageView btnSearch;
    private ImageView btnEndSearch;
    private EditText inputSearch;

    private RelativeLayout toolbarView;
    private ImageView imgPreview;
    private GifImageView imgGif;
    private Bitmap bitmap;
//    private DrawerLayout drwMenu;
    private boolean base = false;
    private boolean search = false;
    private boolean infoMenu = false;

//    private RelativeLayout containerMenu;
    private DrawerLayout menuSliderLeft;
    private RelativeLayout containerViewPager;
    private RelativeLayout containerBaseView;
    private RelativeLayout containerMenuInfoView;
    private RelativeLayout toolbarAllView;
    private ActionBarDrawerToggle mDrawerToggle;
    private SlideMenu adapterMenu;
    private String []textItems;
    private Fragment fragment;
    private String cashTextMenu;

    private ListView menuItemList;

    private String titleMenuName = "Menu name";
    private IAPHelper iapHelper;

    public static void show(Activity activity){
        Intent intent = new Intent(activity, Main.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.zoom_enter_animation, R.anim.zoom_exit_animation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        menuSliderLeft = (DrawerLayout) findViewById(R.id.menu_drawer_layout);
        containerViewPager = (RelativeLayout)findViewById(R.id.container_for_view_pager);
        containerBaseView = (RelativeLayout)findViewById(R.id.container_for_preview);
        containerMenuInfoView = (RelativeLayout)findViewById(R.id.container_for_item_menu);

        setFragment(ViewContent.create(), R.id.container_for_view_pager);
        setFragment(BaseContent.create(), R.id.container_for_preview);

        initializeControls();

        showToolbar();

        showMenuLeftButton();
        showMenuRightButton();
        showCenterInfoView();

        menuItemList = (ListView)findViewById(R.id.menu_left_drawer);
        textItems = getResources().getStringArray(R.array.slider_menu_items_name);
        adapterMenu = new SlideMenu(this, textItems);
        menuItemList.setAdapter(adapterMenu);

        Toolbar bartool = new Toolbar(this);
        View tool = this.getLayoutInflater().inflate(R.layout.i_toolbar_section, null);
        bartool.addView(tool);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                menuSliderLeft,         /* DrawerLayout object */
                bartool,
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                Helper.logOut("MAIN", "Menu Drawer closed ");
                getActionBar().setTitle("Menu 1");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Helper.logOut("MAIN", "Menu drawer open ");
                getActionBar().setTitle("MENU 2");
            }
        };

        initializeListener();
        iapHelper = new IAPHelper(this);
        iapHelper.setListener(this);
        iapHelper.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        iapHelper.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        iapHelper.onActivityResult(requestCode, resultCode, data);
    }

    public IAPHelper getIapHelper() {
        return iapHelper;
    }

    private void setFragment(android.support.v4.app.Fragment fragment, int layoutId){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        if (fragmentManager.findFragmentByTag(tag) == null)
            fragmentManager.beginTransaction().replace(layoutId, fragment, tag).commit();
    }

    private void initializeControls() {

        btnBack = (ImageView) findViewById(R.id.toolbar_btn_back);
        btnMenu = (ImageView) findViewById(R.id.toolbar_btn_menu);
        btnMenuPoint = (ImageView) findViewById(R.id.toolbar_btn_menu_point);
        btnSearch = (ImageView) findViewById(R.id.toolbar_btn_search);
        btnEndSearch = (ImageView) findViewById(R.id.toolbar_btn_end_search);

        inputSearch = (EditText) findViewById(R.id.input_search_request);

        toolbarView = (RelativeLayout) findViewById(R.id.toolbar_main);
        toolbarAllView = (RelativeLayout) toolbarView.findViewById(R.id.toolbar_all_view);

        viewTextInfo = findViewById(R.id.toolbar_center_info_all);
        viewSearch = findViewById(R.id.toolbar_view_center_search);
        viewText = findViewById(R.id.toolbar_view_center_text);

        txtCenterUp = (TextView) findViewById(R.id.toolbar_txt_name_up);
        txtCenterDown = (TextView) findViewById(R.id.toolbar_txt_name_down);
    }

    private void showSearchView(){
        if(search){
            hideSearshView();
        }else {
            search = true;
            Animation animationL = AnimationUtils.loadAnimation(this, R.anim.right_to_left_sliding);
            Animation animationR = AnimationUtils.loadAnimation(this, R.anim.left_to_right_sliding);
//        viewSearch.startAnimation(animationL);
//        viewText.startAnimation(animationR);
            viewSearch.setVisibility(View.VISIBLE);
            viewText.setVisibility(View.GONE);
        }
    }

    private void hideSearshView(){
        search = false;
        Animation animationL = AnimationUtils.loadAnimation(this, R.anim.right_to_left_sliding);
        Animation animationR = AnimationUtils.loadAnimation(this, R.anim.left_to_right_sliding);
//        viewSearch.startAnimation(animationR);
//        viewText.startAnimation(animationL);
        viewText.setVisibility(View.VISIBLE);
        viewSearch.setVisibility(View.GONE);
    }

    private void initializeListener(){
        btnEndSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputSearch.setText("");
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuSliderLeft.openDrawer(Gravity.LEFT);
            }
        });

        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Editable searchString = inputSearch.getText();
                if (actionId == EditorInfo.IME_ACTION_SEARCH && !TextUtils.isEmpty(searchString)) {
                    WallpapersView.search(getApplicationContext(), searchString.toString());
                    return true;
                }
                return false;
            }
        });

        menuItemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case FAVORITES:
                        startViewFavorites();
                        break;
                    case PURCHASE:
                        startViewPurchase();
                        break;
                    case SHARE_WITH_FRIENDS:
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, URL_TO_SHARE);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        break;
                    case INFORMATION:
                        startViewInformation();
                        break;
                }
            }
        });
    }

    private void showCategoriesView(String categoryName){
        setToolbarLabel(categoryName);
        hideSearchInfoView();
        showTextInfoView();
    }

    private void showWallpapersView(String categoriesName){
        setToolbarLabel(categoriesName);
        hideSearchInfoView();
        showTextInfoView();
    }

    private void showBaseContent(ArrayList<ModelWallpaper> wallpapers, int position){
        base = true;
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.left_to_right_sliding);
        containerBaseView.setVisibility(View.VISIBLE);
        containerBaseView.startAnimation(animation);
        containerViewPager.setVisibility(View.GONE);
        containerMenuInfoView.setVisibility(View.GONE);
        toolbarView.setBackgroundColor(getResources().getColor(R.color.color_background_transparent));
        hideSearchButton();
        if(base){
            FragmentManager fragmentManager = getSupportFragmentManager();
            for(int p=0, l=fragmentManager.getFragments().size(); p<l; p++) {
                android.support.v4.app.Fragment fragment = fragmentManager.getFragments().get(p);
                if(fragment !=null && fragment instanceof BaseContent) {
                    ((BaseContent) fragment).setImageResource(wallpapers, position);
                }
            }
        }
    }

    private void showPageContent(){
        base = false;
        containerBaseView.setVisibility(View.GONE);
        containerViewPager.setVisibility(View.VISIBLE);
        containerMenuInfoView.setVisibility(View.GONE);
        toolbarView.setBackgroundColor(getResources().getColor(R.color.color_background));
        showSearchButton();
        FragmentManager fragmentManager = getSupportFragmentManager();
        for(int p=0, l=fragmentManager.getFragments().size(); p<l; p++) {
            android.support.v4.app.Fragment fragment = fragmentManager.getFragments().get(p);
            if(fragment !=null && fragment instanceof ViewContent) {
                Fragment activeFragment = ((ViewContent) fragment).getActiveFragment();
                if(activeFragment != null && activeFragment instanceof WallpapersView)
                    setToolbarLabel((((WallpapersView) activeFragment).getLabelName()));
                else if(activeFragment != null && activeFragment instanceof CategoriesView)
                    setToolbarLabel((((CategoriesView) activeFragment).getLabelName()));
            }
        }
    }

    private Fragment setActualFragment(String className){
        FragmentManager fragmentManager = getSupportFragmentManager();
        for(int p=0, l=fragmentManager.getFragments().size(); p<l; p++) {
            android.support.v4.app.Fragment fragment = fragmentManager.getFragments().get(p);
            String tag = fragment.getTag();
            String nmn = fragment.getClass().getName();
            if(nmn.equals(className)){
                return fragment;
            }
        }
        return null;
    }

    public void onClick(View view) {

    }

    private void showWallpaperDetails(String nameCategory){
        setToolbarLabel(nameCategory);
    }

    @Override
    public void onListenerEnableFragment(int page, String nameCategory) {
        if(base){
            showWallpaperDetails(nameCategory);
        }else {
            switch (page) {
                case ViewContent.VIEW_WALLPAPERS:
                    showWallpapersView(nameCategory);
                    break;
                case ViewContent.VIEW_CATEGORIES:
                    showCategoriesView(nameCategory);
                    break;
            }
        }
    }

    @Override
    public void onListenerWallpaperSelectItem(ArrayList<ModelWallpaper> wallpaper, int position) {
        showBaseContent(wallpaper, position);
        showBackButton();
        hideMenuLeftButton();
    }

    @Override
    public void onListenerBackClick() {
        if(infoMenu){
            disableInfoView();
            return;
        }
        showPageContent();
        hideBackButton();
        showMenuLeftButton();
    }

    @Override
    public void onBackPressed() {

        if(base){
            showPageContent();
            return;
        }else if(infoMenu){
            disableInfoView();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public void onListenerSearchView() {
        showSearchView();
    }

    @Override
    public void onListenerSelectedItem(ModelCategories category) {
        Fragment fragment = setActualFragment(WallpapersView.class.getName());
        if(fragment != null && fragment instanceof WallpapersView){
            ((WallpapersView)fragment).startLoadCategory(category);
        }
//        onListenerEnableFragment(ViewContent.VIEW_WALLPAPERS, category.getNameCategory());
        Fragment fragmentView = setActualFragment(ViewContent.class.getName());
        if(fragmentView != null && fragmentView instanceof ViewContent){
            ((ViewContent)fragmentView).enableWallpaperGrig(category);
            setToolbarLabel(category.getNameCategory());
        }
    }

    @Override
    public void onCategoriesLoaded(List<ModelCategories> categories) {

    }

    private void selectListView(String name, final String key){
        Fragment fragment = setActualFragment(WallpapersView.class.getName());
        if(fragment != null && fragment instanceof WallpapersView){
            ((WallpapersView)fragment).startVisibilityCategory(PreferenceHelper.getStringDataList(this, key));
            ((WallpapersView)fragment).setLabelName(name);
            setToolbarLabel(name);
        }

        Fragment fragmentView = setActualFragment(ViewContent.class.getName());
        if(fragmentView != null && fragmentView instanceof ViewContent){
            ((ViewContent)fragmentView).selectWallpaperGridView();
        }

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.left_to_right_sliding);
        containerBaseView.setVisibility(View.GONE);
        containerViewPager.setVisibility(View.VISIBLE);
        containerViewPager.setAnimation(animation);
        containerViewPager.startAnimation(animation);
        containerMenuInfoView.setVisibility(View.GONE);

        hideBackButton();
        showMenuLeftButton();
    }

    private void startViewFavorites(){
        menuSliderLeft.closeDrawer(Gravity.LEFT);
        selectListView("Favorites", PreferenceHelper.LIKE_LIST_KEY);
    }
    private void startViewPurchase(){
        menuSliderLeft.closeDrawer(Gravity.LEFT);
        selectListView("Purchase", PreferenceHelper.PURCHASE_LIST_KEY);
    }
    private void startViewShared(){
        menuSliderLeft.closeDrawer(Gravity.LEFT);
    }
    private void startViewInformation(){
        infoMenu = true;
        menuSliderLeft.closeDrawer(Gravity.LEFT);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.left_to_right_sliding);
        containerBaseView.setVisibility(View.GONE);
        containerViewPager.setVisibility(View.GONE);
        containerMenuInfoView.setVisibility(View.VISIBLE);
        containerMenuInfoView.setAnimation(animation);
        containerMenuInfoView.startAnimation(animation);

        fragment = Information.create();
        setFragment(fragment, R.id.container_for_item_menu);
        cashTextMenu = txtCenterUp.getText().toString();
        setToolbarLabel("Information");
        hideMenuLeftButton();
        showBackButton();
        hideSearchButton();
        hideMenuRightButton();
    }

    private void disableInfoView(){
        infoMenu = false;
        if(base){
            containerBaseView.setVisibility(View.VISIBLE);
        }else{
            containerViewPager.setVisibility(View.VISIBLE);
        }
        containerMenuInfoView.setVisibility(View.GONE);
        hideBackButton();
        showSearchButton();
        showMenuRightButton();
        showMenuLeftButton();
        setToolbarLabel(cashTextMenu);
        cashTextMenu = null;
    }

    private void removeFragment(android.support.v4.app.Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        if (fragmentManager.findFragmentByTag(tag) != null)
            fragmentManager.beginTransaction().remove(fragment).commit();
    }

    public void showToolbar(){
        toolbarView.setVisibility(View.VISIBLE);
    }
    public void hideToolbar(){
        toolbarView.setVisibility(View.GONE);
    }

    public void showBackButton(){
        btnBack.setVisibility(View.VISIBLE);
    }
    public void hideBackButton(){
        btnBack.setVisibility(View.GONE);
    }

    public void showSearchButton(){
        btnSearch.setVisibility(View.VISIBLE);
    }
    public void hideSearchButton(){
        btnSearch.setVisibility(View.GONE);
    }

    public void showMenuLeftButton(){
        btnMenu.setVisibility(View.VISIBLE);
    }
    public void hideMenuLeftButton(){
        btnMenu.setVisibility(View.GONE);
    }
    public void showMenuRightButton(){
        btnMenuPoint.setVisibility(View.VISIBLE);
    }

    public void hideMenuRightButton(){
        btnMenuPoint.setVisibility(View.GONE);
    }

    public void showCenterInfoView(){
        viewTextInfo.setVisibility(View.VISIBLE);
    }
    public void hideCenterInfoView(){
        viewTextInfo.setVisibility(View.GONE);
    }

    public void showSearchInfoView(){
        viewSearch.setVisibility(View.VISIBLE);
    }
    public void hideSearchInfoView(){
        viewSearch.setVisibility(View.GONE);
    }

    public void showTextInfoView(){
        viewText.setVisibility(View.VISIBLE);
    }
    public void hideTextInfoView(){
        viewText.setVisibility(View.GONE);
    }

    public void setToolbarLabel(String text){
        txtCenterUp.setText(text);
    }

    @Override
    public void bought(String skuToBuy) {
        Intent intent = new Intent(Main.BOUGHT_WALLPAPER_ACTION);
        intent.putExtra(Main.BOUGHT_PRODUCT_ID, skuToBuy);
        sendBroadcast(intent);
    }

    /*
    * drawer on item click
    * */

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        Helper.logOut("MAIN", "Menu Item Drawer Click " + position);

        // Create a new fragment and specify the planet to show based on position
//        Fragment fragment = new PlanetFragment();
//        Bundle args = new Bundle();
//        args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
//        fragment.setArguments(args);
//
//        // Insert the fragment by replacing any existing fragment
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.content_frame, fragment)
//                .commit();
//
//        // Highlight the selected item, update the title, and close the drawer
//        mDrawerList.setItemChecked(position, true);
//        setTitle(mPlanetTitles[position]);
//        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
//        mTitle = title;
        getActionBar().setTitle("TITLE 101");
    }



}
