package com.gulievferdihotmail.wellpaperslife.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;

import com.gulievferdihotmail.wellpaperslife.Fragment.CategoriesView;
import com.gulievferdihotmail.wellpaperslife.Fragment.WallpapersView;

import java.util.ArrayList;


public class TabWidgetCustom extends FragmentPagerAdapter
        implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {


    private final Context mContext;

    private final TabHost mTabHost;

    private final ViewPager mViewPager;

    ArrayList<TabInfo> mTabs = new ArrayList<>();

    static final class TabInfo {

        private final String tag;
        private final Class<?> clss;
        private final Bundle args;

        TabInfo(final String _tag, final Class<?> _class, final Bundle _args) {
            tag = _tag;
            clss = _class;
            args = _args;
        }
    }

    static class DummyTabFactory implements TabHost.TabContentFactory {

        private final Context mContext;



        public DummyTabFactory(final Context context) {

            mContext = context;

        }



        @Override

        public View createTabContent(final String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }



    public TabWidgetCustom(FragmentActivity activity, final TabHost tabHost, final ViewPager viewPager){
        super(activity.getSupportFragmentManager());
        this.mContext = activity;
        this.mTabHost = tabHost;
        this.mViewPager = viewPager;
        this.mTabHost.setOnTabChangedListener(this);
//        this.mViewPager.setAdapter(this);
        this.mViewPager.setOnPageChangeListener(this);
    }

    public void addTab(final TabHost.TabSpec tabSpec, final Class<?> clss, final Bundle args) {

        tabSpec.setContent(new DummyTabFactory(this.mContext));
        String tag = tabSpec.getTag();
        TabInfo info = new TabInfo(tag, clss, args);
        this.mTabs.add(info);
        this.mTabHost.addTab(tabSpec);
        notifyDataSetChanged();
    }


    @Override
    public Fragment getItem(int i) {
        TabInfo info ;
        return Fragment.instantiate(mContext, mTabs.get(i).clss.getName(), mTabs.get(i).args);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Title " + position;
    }

    @Override
    public void onTabChanged(String tabId) {
        int position = mTabHost.getCurrentTab();
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        TabWidget widget = mTabHost.getTabWidget();
        int oldFocusability = widget.getDescendantFocusability();
        widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        mTabHost.setCurrentTab(position);
        widget.setDescendantFocusability(oldFocusability);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
