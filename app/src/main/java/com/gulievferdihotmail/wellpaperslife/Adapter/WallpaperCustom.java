package com.gulievferdihotmail.wellpaperslife.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.gulievferdihotmail.wellpaperslife.Fragment.CategoriesView;
import com.gulievferdihotmail.wellpaperslife.Fragment.WallpapersView;

import java.util.ArrayList;


public class WallpaperCustom extends FragmentPagerAdapter {

    ArrayList<Fragment> fragments = new ArrayList<>();

    public WallpaperCustom(FragmentManager fm){
        super(fm);
        this.fragments.add(CategoriesView.create());
        this.fragments.add(WallpapersView.create());
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                return fragments.get(0);
            case 1:
                return fragments.get(1);
            default:
                return null;
        }
    }

    public Fragment getFragment(int count){return fragments.get(count);}

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Title " + position;
    }
}
