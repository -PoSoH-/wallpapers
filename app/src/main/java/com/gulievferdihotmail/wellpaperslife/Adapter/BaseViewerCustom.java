package com.gulievferdihotmail.wellpaperslife.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.gulievferdihotmail.wellpaperslife.Fragment.BaseImages;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;

import java.util.ArrayList;


public class BaseViewerCustom extends FragmentStatePagerAdapter {

    ArrayList<ModelWallpaper> collection;
    Fragment fragment = null;

    public BaseViewerCustom(FragmentManager fm, ArrayList<ModelWallpaper> collection){
        super(fm);
        this.collection = collection;
    }

    @Override
    public Fragment getItem(int i) {
        String url = collection.get(i).getImageURL();
        fragment = BaseImages.create(url);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public int getCount() {
        return collection.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Title " + position;
    }
}
