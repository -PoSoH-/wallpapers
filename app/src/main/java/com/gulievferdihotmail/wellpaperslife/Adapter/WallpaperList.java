package com.gulievferdihotmail.wellpaperslife.Adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;
import com.gulievferdihotmail.wellpaperslife.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import weborb.reader.DateReader;

/**
 * Created by ADMIN on two.03.2016.
 */
public class WallpaperList extends BaseAdapter {

    public static final double IMAGE_ASPECT_RATIO = 1.7;
//    int []info;
    ArrayList<ModelWallpaper> wallpaper = new ArrayList<>();
    Activity activity;

    public WallpaperList(Activity activity, ArrayList<ModelWallpaper> wallpaper){
        super();
        this.activity = activity;
        this.wallpaper = wallpaper;
    }

    @Override
    public int getCount() {
        return wallpaper.size();
    }

    @Override
    public Object getItem(int position) {
        return wallpaper.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = activity
                    .getLayoutInflater().inflate(R.layout.i_grid_wallpaper, null, false);
            viewHolder = new ViewHolder((ImageView)convertView
                    .findViewById(R.id.item_grid_wallpaper_image));
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
        int width = (int) ((metrics.widthPixels / 2)*0.5);
        int height = (int) (width * IMAGE_ASPECT_RATIO);

        Picasso.with(activity)
                .load(wallpaper.get(position).getImageURL())
                .resize(width, height)
                .centerCrop()
                .into(viewHolder.imgView);

        return convertView;
    }

    private static class ViewHolder {
        private ImageView imgView;

        public ViewHolder(ImageView img){
            this.imgView = img;
        }
    }

    public void addWallpaperList(ArrayList<ModelWallpaper> wallpaper){
        this.wallpaper = wallpaper;
        notifyDataSetChanged();
    }

    public void removeItems(){
        this.wallpaper = new ArrayList<ModelWallpaper>();
        notifyDataSetChanged();
    }
}
