package com.gulievferdihotmail.wellpaperslife.Adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Models.ModelCategories;
import com.gulievferdihotmail.wellpaperslife.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADMIN on two.03.2016.
 */
public class CategoryList extends BaseAdapter {

    ArrayList<ModelCategories> categories = new ArrayList<>();
//    ArrayList<String> value = new ArrayList<>();
    Activity activity;

    public CategoryList(Activity activity, List<ModelCategories> categories){
        super();
        this.activity = activity;
        this.categories = (ArrayList<ModelCategories>) categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = (RelativeLayout)activity
                    .getLayoutInflater().inflate(R.layout.i_list_categories, null, false);
//            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder = new ViewHolder((ImageView)convertView.findViewById(R.id.item_categories_image)
                    ,(TextView)convertView.findViewById(R.id.item_categories_text_base)
                    ,(TextView)convertView.findViewById(R.id.item_categories_text_value));
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(this.activity)
                .load(categories.get(position).getImageURL())
                .centerCrop()
                .fit()
                .into(viewHolder.imgView);
//        viewHolder.imgView.setImageResource(R.drawable.cancel);
        viewHolder.txtBaseText.setText(categories.get(position).getNameCategory());
//        viewHolder.txtValueText.setText("("+categories.get(position)+")");

        return convertView;
    }

    public void addCategories(ArrayList<ModelCategories>categories){
        this.categories = categories;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        private ImageView imgView;
        private TextView txtBaseText;
        private TextView txtValueText;

        public ViewHolder(ImageView img, TextView txt1, TextView txt2){
            this.imgView = img;
            this.txtBaseText = txt1;
            this.txtValueText = txt2;
        }
    }
}
