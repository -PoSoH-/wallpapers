package com.gulievferdihotmail.wellpaperslife.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.R;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by ADMIN on 14.03.2016.
 */
public class SlideMenu extends BaseAdapter {

    private Context context;
    private String[] textItems;
    private int []imgItems;

    public SlideMenu(Context context, String []textItems) {
        super();
        this.context = context;
        this.textItems = textItems;
        imgItems = new int[] {
//            R.drawable.categories,
//            R.drawable.wallpapers,
            R.drawable.favorite,
            R.drawable.purchase,
            R.drawable.share,
            R.drawable.information,
        };
    }

    @Override
    public int getCount() {
        return this.textItems.length;
    }

    @Override
    public Object getItem(int position) {
        return textItems[position];
    }

    public String getItemString(int position) {
        return (String)textItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.i_text_sliding, null);
            viewHolder = new ViewHolder(convertView.findViewById(R.id.txt_sliding_item_menu)
                    , convertView.findViewById(R.id.img_sliding_item_menu));
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textMenuView.setText(this.textItems[position]);
        viewHolder.imageMenuView.setImageResource(imgItems[position]);

        return convertView;
    }

    private static class ViewHolder {

        public ImageView imageMenuView;
        public TextView textMenuView;

        public ViewHolder(View textMenuView, View imageMenuView){
            this.textMenuView = (TextView) textMenuView;
            this.imageMenuView = (ImageView) imageMenuView;
        }
    }
}
