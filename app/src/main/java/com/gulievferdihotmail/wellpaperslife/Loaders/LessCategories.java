package com.gulievferdihotmail.wellpaperslife.Loaders;

import android.graphics.AvoidXfermode;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.gulievferdihotmail.wellpaperslife.Fragment.CategoriesView;
import com.gulievferdihotmail.wellpaperslife.Models.ModelCategories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ADMIN on 01.03.2016.
 */
public class LessCategories {

    public static final String TABLE_CATEGORIES = "Categories";
    public static final String TC_IMAGE_URL = "imageUrl";
    public static final String TC_OBJECT_ID = "objectId";
    public static final String TC_NAME = "name";
    public static final String TC_PAY = "pay";
    public static final String TC_PRODUCT_ID = "productId";

    public static void loadCategories(final OnCallBackCategoriesListener callBackListener){

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        QueryOptions options = new QueryOptions();
        options.setPageSize(100);
        dataQuery.setQueryOptions(options);
        dataQuery.addProperty(TC_IMAGE_URL);
        dataQuery.addProperty(TC_OBJECT_ID);
        dataQuery.addProperty(TC_NAME);
        dataQuery.addProperty(TC_PAY);
        dataQuery.addProperty(TC_PRODUCT_ID);

        Backendless.Persistence.of(TABLE_CATEGORIES).find(dataQuery, new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> mapBackendlessCollection) {
                ArrayList<ModelCategories> categoriesList = new ArrayList<>();
                if(mapBackendlessCollection != null){
                    ModelCategories categories;
                    List<Map> data =  mapBackendlessCollection.getData();
                    for(Map map : data) {
                        categories = new ModelCategories();
                        String productId = null;
                        Object productIdObject = map.get(TC_PRODUCT_ID);
                        if (productIdObject != null) productId = productIdObject.toString();
                        categories.init(map.get(TC_OBJECT_ID).toString()
                                , map.get(TC_NAME).toString()
                                , map.get(TC_IMAGE_URL).toString()
                                , Boolean.valueOf(map.get(TC_PAY).toString())
                                , productId);
                        categoriesList.add(categories);
                    }
                    callBackListener.onSuccess(categoriesList);
                } else {
                    callBackListener.onError();
                }
            }
            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                callBackListener.onError();
            }
        });
    }

    public interface OnCallBackCategoriesListener{
        void onSuccess(ArrayList<ModelCategories> categories);
        void onError();
    }

}
