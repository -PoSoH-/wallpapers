package com.gulievferdihotmail.wellpaperslife.Loaders;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ADMIN on 01.03.2016.
 */
public class LessWallpaper {

    public static final String TABLE_IMAGES = "Images";
    public static final String TI_CATEGORY = "category";
    public static final String TI_IMAGE_URL = "imageUrl";
    public static final String TI_OBJECT_ID = "objectId";
    public static final String TI_NAME = "name";
    public static final String TI_DOWNLOADS = "downloads";
    public static final String TI_FAVORITES = "likes";
    public static final String TI_PAY = "pay";
    public static final String TI_TAGS = "tags";
    public static final String TI_CREATED = "created";
    public static final String TI_LIVE = "live";
    public static final String TI_PRODUCT_ID = "productId";
    public static final String TI_PRICE = "price";

    public static void loadWallpapers(String searchString, String categoryId, ArrayList<String> objectIds,
                                      final OnCallBackWallpaperListener callBackListener){

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        QueryOptions options = new QueryOptions();
        options.setPageSize(100);
        dataQuery.setQueryOptions(options);
        dataQuery.addProperty(TI_OBJECT_ID);
        dataQuery.addProperty(TI_NAME);
        dataQuery.addProperty(TI_IMAGE_URL);
        dataQuery.addProperty(TI_CATEGORY);
        dataQuery.addProperty(TI_DOWNLOADS);
        dataQuery.addProperty(TI_FAVORITES);
        dataQuery.addProperty(TI_PAY);
        dataQuery.addProperty(TI_CREATED);
        dataQuery.addProperty(TI_TAGS);
        dataQuery.addProperty(TI_LIVE);
        dataQuery.addProperty(TI_PRODUCT_ID);
        dataQuery.addProperty(TI_PRICE);
        if (searchString != null)
            dataQuery.setWhereClause("name LIKE '%" + searchString + "%' OR tags LIKE '%" + searchString + "%'");
        else if (categoryId != null)
            dataQuery.setWhereClause("category.objectId = '" + categoryId + "'");
        else if (objectIds != null) {
            StringBuilder wheareClause = new StringBuilder();
            wheareClause.append("objectId in (");
            int len = objectIds.size();
            for(String id : objectIds) {
                len -= 1;
                if(len == 0){
                    wheareClause.append("'").append(id).append("'");
                }else{
                    wheareClause.append("'").append(id).append("',");
                }
            }
            wheareClause.append(")");
            dataQuery.setWhereClause(wheareClause.toString());
        }

        Backendless.Persistence.of(TABLE_IMAGES).find(dataQuery, new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> mapBackendlessCollection) {
                ArrayList<ModelWallpaper> wallpaperList = new ArrayList<>();
                if(mapBackendlessCollection != null){
                    ModelWallpaper wallpaper;
                    List<Map> data =  mapBackendlessCollection.getData();
                    for(Map map : data) {
                        wallpaper = new ModelWallpaper();

                        HashMap<String, String> category = (HashMap)map.get(TI_CATEGORY);
                        boolean live;
                        if(map.get(TI_LIVE) == null) live = false;
                        else live = Boolean.valueOf(map.get(TI_LIVE).toString());

                        Object productIdObject = map.get(TI_PRODUCT_ID);
                        String productId = null;
                        if (productIdObject != null) productId = productIdObject.toString();

                        double price = 0;
                        Object priceObject = map.get(TI_PRICE);
                        if (priceObject != null) price = Float.parseFloat(priceObject.toString());

                        String tags = "";
                        Object tagsObject = map.get(TI_TAGS);
                        if (tagsObject != null) tags = tagsObject.toString();

                        wallpaper.init(map.get(TI_OBJECT_ID).toString()
                                , map.get(TI_NAME).toString()
                                , map.get(TI_IMAGE_URL).toString()
                                , category.get(TI_OBJECT_ID).toString()
                                , map.get(TI_DOWNLOADS).toString()
                                , map.get(TI_FAVORITES).toString()
                                , new java.util.Date().getTime()
                                , Boolean.valueOf(map.get(TI_PAY).toString())
                                , tags
                                , live
                                , productId
                                , price);
                        wallpaperList.add(wallpaper);
                    }
                    callBackListener.onSuccess(wallpaperList);
                }else{
                    callBackListener.onError();
                }
            }
            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                callBackListener.onError();
            }
        });
    }

    public static void loadWallpaperFavoritesForChange(final boolean value, String objectId){
        Backendless.Persistence.of(Images.class).findById(objectId, new AsyncCallback<Images>() {
            @Override
            public void handleResponse(Images images) {
                int count = images.getLikes();
                if(value) count += 1;
                else count -= 1;
                images.setLikes(count);
                Backendless.Persistence.of(Images.class).save(images, new AsyncCallback<Images>() {
                    @Override
                    public void handleResponse(Images images) {

                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        Helper.logOut("FAVORITES", "ERROR " + backendlessFault);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

            }
        });
    }

    public static void loadWallpaperDownloadsForChange(String objectId) {
        Backendless.Persistence.of(Images.class).findById(objectId, new AsyncCallback<Images>() {
            @Override
            public void handleResponse(Images images) {
                int count = images.getDownloads();
                count += 1;
                images.setDownloads(count);
                Backendless.Persistence.of(Images.class).save(images, new AsyncCallback<Images>() {
                    @Override
                    public void handleResponse(Images images) {

                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        Helper.logOut("FAVORITES", "ERROR " + backendlessFault);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

            }
        });
    }

    public static class Images{
//        private ModelCategories category;
        private String imageUrl;
        private String objectId;
        private String name;
        private int downloads;
        private boolean pay;
        private String tags;
        private java.util.Date created;
        private java.util.Date updated;
        private int likes;
        private boolean live;

//        public ModelCategories getCategory() {
//            return category;
//        }
//
//        public void setCategory(ModelCategories category) {
//            this.category = category;
//        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getObjectId() {
            return objectId;
        }

        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getDownloads() {
            return downloads;
        }

        public void setDownloads(int downloads) {
            this.downloads = downloads;
        }

        public boolean isPay() {
            return pay;
        }

        public void setPay(boolean pay) {
            this.pay = pay;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public java.util.Date getCreated() {
            return created;
        }

        public void setCreated(java.util.Date created) {
            this.created = created;
        }

        public java.util.Date getUpdated() {
            return updated;
        }

        public void setUpdated(java.util.Date updated) {
            this.updated = updated;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }
    }

    public interface OnCallBackWallpaperListener{
        void onSuccess(ArrayList<ModelWallpaper> categories);
        void onError();
    }
}
