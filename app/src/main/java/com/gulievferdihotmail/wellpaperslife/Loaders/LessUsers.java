package com.gulievferdihotmail.wellpaperslife.Loaders;

import com.backendless.Backendless;
import com.backendless.BackendlessSimpleQuery;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserTokenStorageFactory;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;

import java.util.HashMap;

/**
 * Created by ADMIN on 01.03.2016.
 */
public class LessUsers {

    public static final String TABLE_USERS = "USERS";
    public static final String TU_LAST_NAME = "last_name";
    public static final String TU_CITY = "city";
    public static final String TU_FIRST_NAME = "name";
    public static final String TU_EMAIL = "email";
    public static final String TU_PASSWORD = "password";

    public static void registeredUser(HashMap<String, String> dataInfoUser, final OnResponseAccountListener responseListeners){
        BackendlessUser user = new BackendlessUser();
        user.setEmail(dataInfoUser.get(TU_EMAIL));
        user.setPassword(dataInfoUser.get(TU_PASSWORD));
        user.setProperty(TU_FIRST_NAME, dataInfoUser.get(TU_FIRST_NAME));
        user.setProperty(TU_LAST_NAME, dataInfoUser.get(TU_LAST_NAME));
        user.setProperty(TU_CITY, dataInfoUser.get(TU_CITY));

        Backendless.UserService.register(user, new BackendlessCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser backendlessUser) {
                Helper.logOut(LessUsers.class.getName(), backendlessUser.getObjectId());
                responseListeners.onSuccess();
            }
        });
    }

    public static void loginUser(HashMap<String, String> dataUserLogin, final OnResponseAccountListener responseListener){
        Backendless.UserService.login(dataUserLogin.get(TU_EMAIL), dataUserLogin.get(TU_PASSWORD), new BackendlessCallback<BackendlessUser>(){
            @Override
            public void handleResponse(BackendlessUser backendlessUser) {
                responseListener.onSuccess();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                super.handleFault(fault);
                responseListener.onError();
            }
        }, true);

    }

    public static boolean userToken(){
        String userToken = UserTokenStorageFactory.instance().getStorage().get();
        if (userToken != null && userToken.equals(""))
            return true;
        else
            return false;
    }

    public static BackendlessUser fetchUser(){
        return Backendless.UserService.CurrentUser();
    }

    public interface OnResponseAccountListener {
        void onSuccess();
        void onError();
    }

}
