package com.gulievferdihotmail.wellpaperslife;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.gulievferdihotmail.wellpaperslife.Loaders.LessInit;

public class Start extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            IBinder iBinder = currentFocus.getWindowToken();
            if (iBinder != null) inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
        }
    }
}
