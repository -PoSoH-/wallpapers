package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;
import com.gulievferdihotmail.wellpaperslife.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class BaseImages extends Fragment {

    private ImageView image;
    private ProgressBar progress;
    private String urlImages;

    public BaseImages(String url){
        if(url != null)
            urlImages = new String(url);
    }

    public static Fragment create(String url) {return new BaseImages(url);}

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_images_base_view, container, false);
        image = (ImageView)view.findViewById(R.id.img_base_view_container);
        progress = (ProgressBar) view.findViewById(R.id.progress_bar_picasso_base);
        progress.setVisibility(View.VISIBLE);
        setImages(urlImages);
        return view;
    }

    public void setImages(String url){
        if(urlImages!=null) {
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int width = (int) ((metrics.widthPixels / 2) * 0.5);
            int height = (int) (width * 1.55);
            Picasso.with(getActivity())
                    .load(url)
                    .fit()
                    .centerCrop()
                    .into(image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progress.setVisibility(View.GONE);
                        }
                    });
        }
    }
}

