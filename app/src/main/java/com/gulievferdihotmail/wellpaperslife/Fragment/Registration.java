package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessUsers;
import com.gulievferdihotmail.wellpaperslife.R;

import java.util.HashMap;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class Registration extends Fragment {

    private OnRegistrationListener registarationListener;

    private Button btnLogIn;

    private EditText editFirstName;
    private EditText editLastName;
    private EditText editEmailAddress;
    private EditText editCity;
    private EditText editPassword;
    private EditText editCheckPassword;

    private ImageView imgViewOkFirstName;
    private ImageView imgViewOkLastName;
    private ImageView imgViewOkEmailName;
    private ImageView imgViewOkCityName;
    private ImageView imgViewOkPasswordName;
    private ImageView imgViewOkChangePasswordName;

    public static Registration create(){return new Registration();}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnRegistrationListener)
            registarationListener = (OnRegistrationListener) context;
        else
            throw new ClassCastException("Error! Please implements OnRegistrationListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        registarationListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.f_registration, container, false);
        editFirstName = (EditText) view.findViewById(R.id.txt_registration_first_name);
        editLastName = (EditText) view.findViewById(R.id.txt_registration_last_name);
        editEmailAddress = (EditText)view.findViewById(R.id.txt_registration_email_address);
        editCity = (EditText)view.findViewById(R.id.txt_registration_city);
        editPassword = (EditText)view.findViewById(R.id.txt_registration_password);
        editCheckPassword = (EditText)view.findViewById(R.id.txt_registration_check_password);
        btnLogIn = (Button)view.findViewById(R.id.btn_registration);

        imgViewOkFirstName = (ImageView)view.findViewById(R.id.img_registration_first_name_ok);
        imgViewOkLastName = (ImageView)view.findViewById(R.id.img_registration_last_name_ok);
        imgViewOkEmailName  = (ImageView)view.findViewById(R.id.img_registration_email_ok);
        imgViewOkCityName  = (ImageView)view.findViewById(R.id.img_registration_city_ok);
        imgViewOkPasswordName  = (ImageView)view.findViewById(R.id.img_registration_password_ok);
        imgViewOkChangePasswordName  = (ImageView)view.findViewById(R.id.img_registration_change_password_ok);

        editFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Helper.logOut("EditText after change", s.toString());
                if(s.length()>=Helper.MIN_NUMBER_FIRST_NAME) imgViewOkFirstName.setVisibility(View.VISIBLE);
                else imgViewOkFirstName.setVisibility(View.INVISIBLE);
            }
        });

        editLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Helper.logOut("EditText after change", s.toString());
                if(s.length()>=Helper.MIN_NUMBER_LAST_NAME) imgViewOkLastName.setVisibility(View.VISIBLE);
                else imgViewOkLastName.setVisibility(View.INVISIBLE);
            }
        });

        editEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Helper.logOut("EditText after change", s.toString());
                if(registarationListener == null) return;
                if(registarationListener.onListenerValidEmailAddress(editEmailAddress)) imgViewOkEmailName.setVisibility(View.VISIBLE);
                else imgViewOkEmailName.setVisibility(View.INVISIBLE);
            }
        });

        editCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Helper.logOut("EditText after change", s.toString());
                if(s.length()>=Helper.MIN_NUMBER_CITY_NAME) imgViewOkCityName.setVisibility(View.VISIBLE);
                else imgViewOkCityName.setVisibility(View.INVISIBLE);
            }
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Helper.logOut("EditText after change", s.toString());
                if(s.length()>=Helper.MIN_NUMBER_PASSWORD) imgViewOkPasswordName.setVisibility(View.VISIBLE);
                else imgViewOkPasswordName.setVisibility(View.INVISIBLE);
            }
        });

        editCheckPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Helper.logOut("EditText after change", s.toString());
                if(s.length()>=Helper.MIN_NUMBER_PASSWORD) imgViewOkChangePasswordName.setVisibility(View.VISIBLE);
                else imgViewOkChangePasswordName.setVisibility(View.INVISIBLE);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialiseButtonClick();
    }

    private void initialiseButtonClick(){
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.logOut("LOGIN", "login button");
                if(registarationListener != null){
                    boolean firstNameEmpty = registarationListener.onListenerEmptyField(editFirstName);
                    boolean lastNameEmpty = registarationListener.onListenerEmptyField(editLastName);
                    boolean emailEmpty = registarationListener.onListenerEmptyField(editEmailAddress);
                    boolean cityEmpty = registarationListener.onListenerEmptyField(editCity);
                    boolean passwordEmpty = registarationListener.onListenerEmptyField(editPassword);
                    boolean passwordCheckEmpty = registarationListener.onListenerEmptyField(editCheckPassword);
                    boolean emailCorrectly;
                    boolean passwordCorrectly;
                    boolean passwordCheckCorrectly;
                    if(!firstNameEmpty){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_name_first)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }
                    if(!lastNameEmpty){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_name_last)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }
                    if(!emailEmpty){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_email)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }else{
                        emailCorrectly = registarationListener.onListenerValidEmailAddress(editEmailAddress);
                        if(!emailCorrectly){
                            ErrorMessage.show(getActivity()
                                    , getString(R.string.txt_error_email_correctly)
                                    , ErrorMessage.DIALOG_TYPE_ERROR);
                            return;
                        }
                    }
                    if(!cityEmpty){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_city)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }
                    if(!passwordEmpty){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_password)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }else{
                        passwordCorrectly = registarationListener.onListenerValidNumberPassword(editPassword);
                        if(!passwordCorrectly){
                            ErrorMessage.show(getActivity()
                                    , getString(R.string.txt_error_short_password)
                                    , ErrorMessage.DIALOG_TYPE_ERROR);
                            return;
                        }
                    }
                    if(!passwordCheckEmpty){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_check_password)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }else{
                        passwordCheckCorrectly = registarationListener.onListenerValidNumberPassword(editCheckPassword);
                        if(!passwordCheckCorrectly){
                            ErrorMessage.show(getActivity()
                                    , getString(R.string.txt_error_short_password_check)
                                    , ErrorMessage.DIALOG_TYPE_ERROR);
                            return;
                        }
                    }
                    if(firstNameEmpty && lastNameEmpty && emailEmpty && cityEmpty
                            && passwordEmpty && passwordCheckEmpty
                            && emailCorrectly && passwordCorrectly && passwordCheckCorrectly){
                        if(registarationListener.onListenerCheckPassword(editPassword, editCheckPassword)) {
                            HashMap<String, String> infoUsers = new HashMap<>();
                            infoUsers.put(LessUsers.TU_FIRST_NAME, editFirstName.getText().toString());
                            infoUsers.put(LessUsers.TU_LAST_NAME, editLastName.getText().toString());
                            infoUsers.put(LessUsers.TU_EMAIL, editEmailAddress.getText().toString());
                            infoUsers.put(LessUsers.TU_CITY, editCity.getText().toString());
                            infoUsers.put(LessUsers.TU_PASSWORD, editPassword.getText().toString());

                            LessUsers.registeredUser(infoUsers, new LessUsers.OnResponseAccountListener() {
                                @Override
                                public void onSuccess() {
                                    registarationListener.onListenerStartNextScreen(Helper.TYPE_MAIN);
                                }

                                @Override
                                public void onError() {
                                    ErrorMessage.show(getActivity()
                                            , "Registration not success!.."
                                            , ErrorMessage.DIALOG_TYPE_ERROR);
                                }
                            });
                        }
                        else
                            ErrorMessage.show(getActivity()
                                    , getString(R.string.txt_error_password_not_match)
                                    , ErrorMessage.DIALOG_TYPE_ERROR);
                    }
                }
            }
        });
    }

    public interface OnRegistrationListener {
        boolean onListenerEmptyField(EditText editText);
        boolean onListenerValidEmailAddress(EditText editText);
        boolean onListenerValidNumberPassword(EditText editText);
        void onListenerStartNextScreen(String typeNextScreen);
        boolean onListenerCheckPassword(EditText password, EditText CheckPassword);
    }
}


