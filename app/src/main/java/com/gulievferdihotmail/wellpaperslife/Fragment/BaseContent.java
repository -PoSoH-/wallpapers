package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gulievferdihotmail.wellpaperslife.Activity.Main;
import com.gulievferdihotmail.wellpaperslife.Adapter.BaseViewerCustom;
import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Helpers.PreferenceHelper;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessWallpaper;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;
import com.gulievferdihotmail.wellpaperslife.R;
import com.gulievferdihotmail.wellpaperslife.Services.WallpaperGIF;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class BaseContent extends Fragment {

    //    @Bind(R.id.img_base_content_view) ImageView viewWallpaper;
    @Bind(R.id.download_layout) View downloadLayout;
    @Bind(R.id.view_wallpaper_txt_image_name) TextView txtNameWallpaper;
    @Bind(R.id.view_wallpaper_txt_image_count) TextView txtCountWallpaper;
    @Bind(R.id.view_wallpaper_txt_like) TextView txtCountLikes;
    @Bind(R.id.view_wallpaper_txt_downloads) TextView txtCountDownloads;
    @Bind(R.id.txt_view_wallpaper_tag) TextView txtTagsList;
    @Bind(R.id.txt_downloads_marker) TextView btnDownload;
    @Bind(R.id.txt_set_as_wallpaper_btn) TextView btnSetAsWallpaper;
    @Bind(R.id.progress_bar_view) ProgressBar progressBar;
    @Bind(R.id.view_wallpaper_like_icon_no) ImageView likeNoView;
    @Bind(R.id.view_wallpaper_like_icon_yes) ImageView likeYesView;
    @Bind(R.id.download_bt) ImageView download;
    @Bind(R.id.download_bt_active) ImageView downloadActive;
    @Bind(R.id.view_wallpaper_img_photo) ImageView shareInstagram;

    private ProgressDialog pDialog;
    private BaseViewerCustom adapterPage;
    private ViewPager viewPageBase;

    private int pagePosition = 0;
    private ArrayList<String> likes = new ArrayList<>();
    private ArrayList<String> downloads = new ArrayList<>();

    public static final int progress_bar_type = 0;
    public static final int WALLPAPER_DETAILS = 3;

    public static BaseContent create() {
        return new BaseContent();
    }
    private OnBaseViewListener baseListener;
    private ArrayList<ModelWallpaper> wallpapers;

    private final String STATIC = "/static/";
    private final String DYNAMIC = "/dynamic/";
    private final String JPG = ".jpg";
    private final String GIF = ".gif";
    private String pathStatic;
    private String pathDynamic;
    private boolean errorLoad = false;
    private File tempFile;

    private BroadcastReceiver buyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String boughtProductId = intent.getStringExtra(Main.BOUGHT_PRODUCT_ID);
            ModelWallpaper currentWallpaper = getCurrentWallpaper();
            if (currentWallpaper != null && currentWallpaper.get_ProductId().equals(boughtProductId)) {
                ArrayList<String> purchasedIds = PreferenceHelper.getStringDataList(getContext(),
                        PreferenceHelper.PURCHASE_LIST_KEY);
                if (purchasedIds == null) purchasedIds = new ArrayList<>();
                purchasedIds.add(currentWallpaper.getObjectId());
                PreferenceHelper.writeStringDataList(getContext(), PreferenceHelper.PURCHASE_LIST_KEY,
                        purchasedIds);
                downloadWallpaper(currentWallpaper);
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(buyBroadcastReceiver, new IntentFilter(Main.BOUGHT_WALLPAPER_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(buyBroadcastReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnBaseViewListener)
            baseListener = (OnBaseViewListener) context;
        else
            throw new ClassCastException("Error! Please implements OnWallpapersListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        baseListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_wallpaper_view, container, false);
        ButterKnife.bind(this, view);
        likes = PreferenceHelper.getStringDataList(getActivity(), PreferenceHelper.LIKE_LIST_KEY);
        downloads = PreferenceHelper.getStringDataList(getActivity(), PreferenceHelper.DOWNLOADS_LIST_KEY);
        return view;
    }

    private boolean hasLikes(String idObjects){
        if(likes!=null) {
            for (String idLike : likes) {
                if (idLike.equals(idObjects)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ImageView btnBack = (ImageView)getActivity().findViewById(R.id.toolbar_btn_back);

        pathStatic = getActivity().getFilesDir().getAbsolutePath() + STATIC;
        pathDynamic = getActivity().getFilesDir().getPath() + DYNAMIC;

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (baseListener != null) baseListener.onListenerBackClick();
            }
        });
    }

//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case progress_bar_type: // we set this to 0
//                pDialog = new ProgressDialog(getActivity());
//                pDialog.setMessage("Downloading file. Please wait...");
//                pDialog.setIndeterminate(false);
//                pDialog.setMax(100);
//                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                pDialog.setCancelable(true);
//                pDialog.show();
//                return pDialog;
//            default:
//                return null;
//        }
//    }

    @OnClick(R.id.download_bt) void downloadClicked() {
        if (downloadLayout.getVisibility() == View.GONE) {
            downloadLayout.setVisibility(View.VISIBLE);
            downloadLayout.setAlpha(0);
            ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(downloadLayout, View.ALPHA, 0, 1);
            alphaAnimation.start();
        }
        else {
            ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(downloadLayout, View.ALPHA, 1, 0);
            alphaAnimation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {}

                @Override
                public void onAnimationEnd(Animator animation) {
                    downloadLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {}

                @Override
                public void onAnimationRepeat(Animator animation) {}
            });
            alphaAnimation.start();
        }
    }

    @OnClick(R.id.txt_downloads_marker) void downloadClickPressed() {
        ModelWallpaper wallpaper = getCurrentWallpaper();
        if (wallpaper.isPay()) ((Main) getActivity()).getIapHelper().checkIfBoughtOrBuy(wallpaper.get_ProductId());
        else downloadWallpaper(wallpaper);
    }

    private void downloadWallpaper(ModelWallpaper wallpaper) {
        DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL(wallpaper);
        downloadFileFromURL.doInBackground();
        downloadFileFromURL.execute(wallpaper.getImageURL());
    }

    private ModelWallpaper getCurrentWallpaper() {
        if (wallpapers != null) return wallpapers.get(viewPageBase.getCurrentItem());
        return null;
    }

    @OnClick(R.id.txt_set_as_wallpaper_btn) void setWallpaperPressed() {
        File file;
        if(wallpapers.get(pagePosition).getLive()){
            file = new File(getActivity().getFilesDir(), wallpapers.get(pagePosition).getObjectId() + GIF);
            PreferenceHelper.setStringAbsolutePath(getActivity(), wallpapers.get(pagePosition).getObjectId()); //file.getAbsolutePath());
            Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
                    new ComponentName(getActivity(), WallpaperGIF.class));
            startActivity(intent);

        }else{
            WallpaperManager myWallpaperManager
                    = WallpaperManager.getInstance(getActivity().getApplicationContext());
            try {
                file = new File(getActivity().getFilesDir(), wallpapers.get(pagePosition).getObjectId() + JPG);
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                if(bitmap != null)
                    myWallpaperManager.setBitmap(bitmap);
                else
                    ErrorMessage.show(getActivity(), "ErrorLoad file", ErrorMessage.DIALOG_TYPE_ERROR);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.view_wallpaper_like_icon_no) void onClickSetLike(){
        if(likes == null) likes = new ArrayList<>();
        likes.add(wallpapers.get(pagePosition).getObjectId());
        int count = Integer.valueOf(txtCountLikes.getText().toString());
        count++;
        txtCountLikes.setText(String.valueOf(count));
        wallpapers.get(pagePosition).setLikes(count);
        PreferenceHelper.writeStringDataList(getActivity(), PreferenceHelper.LIKE_LIST_KEY, likes);
        likeYesView.setVisibility(View.VISIBLE);
        likeNoView.setVisibility(View.GONE);
        LessWallpaper.loadWallpaperFavoritesForChange(true, wallpapers.get(pagePosition).getObjectId());
    }

    @OnClick(R.id.view_wallpaper_like_icon_yes) void onClickOutLike(){
        if(likes == null) return;
        likes.remove(wallpapers.get(pagePosition).getObjectId());
        PreferenceHelper.writeStringDataList(getActivity(), PreferenceHelper.LIKE_LIST_KEY, likes);
        int count = Integer.valueOf(txtCountLikes.getText().toString());
        count--;
        txtCountLikes.setText(String.valueOf(count));
        wallpapers.get(pagePosition).setLikes(count);
        PreferenceHelper.writeStringDataList(getActivity(), PreferenceHelper.LIKE_LIST_KEY, likes);
        likeYesView.setVisibility(View.GONE);
        likeNoView.setVisibility(View.VISIBLE);
        LessWallpaper.loadWallpaperFavoritesForChange(false, wallpapers.get(pagePosition).getObjectId());
    }

    @OnClick(R.id.view_wallpaper_img_photo) void shareInstagrammClick(){

        Intent share = new Intent(Intent.ACTION_SEND);
        String type = "image/*";
        String filename = wallpapers.get(pagePosition).getObjectId();
        String path = getActivity().getFilesDir().toString();
        String fullpath;
        if(wallpapers.get(pagePosition).getLive()) {
            filename = filename + GIF;
        }
        else {
            filename = filename + JPG;
        }
        fullpath = path + "/" + filename;
        File file = new File(fullpath);
        Uri uri =  Uri.fromFile(file);

        File file2 = new File(String.valueOf(Environment.getExternalStorageDirectory()));
        file2 = new File(file2.getAbsolutePath()); // + "/" + "temp");
//        file2.mkdir();
        String pathCreate = "temp.jpg";
        tempFile = new File(file2, pathCreate);

        share.setType(type);

        FileInputStream is = null;
        try {
            is = new FileInputStream(file.getAbsolutePath());
        }catch (Exception e){

        }

        byte[] bytes = new byte[(int) file.length()];
        int offset = 0;
        int numRead = 0;
        try {
            while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Uri uri2 =  Uri.fromFile(tempFile);

        OutputStream fOut = null;
        try {
            fOut = new FileOutputStream(tempFile);
            fOut.write(bytes);
            fOut.flush();
            fOut.close();
        }
        catch (Exception e) // здесь необходим блок отслеживания реальных ошибок и исключений, общий Exception приведен в качестве примера
        {
            e.getMessage();
        }

        share.putExtra(Intent.EXTRA_STREAM, uri2);
        this.startActivityForResult(share.createChooser(share, "Share to"), 1111);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            Log.v("Instagram", "shared ok");
            if(requestCode == 1111){
                tempFile.delete();
            }
        } else if (resultCode == getActivity().RESULT_CANCELED) {
            Toast.makeText(getActivity(), "Sharing is canceled!", Toast.LENGTH_SHORT).show();
            tempFile.delete();
        }
    }


    private void viewPagerListenerCreate(){
        viewPageBase.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pagePosition = viewPageBase.getCurrentItem();
                setData();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void removeFragmentManager(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        int count = fragmentManager.getFragments().size()-1;
        while (count >= 0){
            if(fragmentManager.getFragments().get(count) instanceof BaseImages){
                fragmentManager.getFragments().remove(count);
            }
            count --;
        }
    }

    public void setImageResource(ArrayList<ModelWallpaper> wallpapers, int position){
        this.wallpapers = wallpapers;
        pagePosition = position;
        adapterPage = new BaseViewerCustom(getActivity().getSupportFragmentManager(), this.wallpapers);
        viewPageBase = (ViewPager) getActivity().findViewById(R.id.view_pager_base_content);
        viewPageBase.setAdapter(adapterPage);
        viewPageBase.setCurrentItem(pagePosition);
        viewPagerListenerCreate();
        setData();
    }

    private void setData(){
        if(baseListener != null){
            baseListener.onListenerEnableFragment(WALLPAPER_DETAILS, wallpapers.get(pagePosition).getNameCategory());
        }
        ModelWallpaper wallpaper = wallpapers.get(pagePosition);
        txtNameWallpaper.setText(wallpaper.getNameCategory());
        txtCountDownloads.setText(wallpaper.getDownloads());
        txtCountLikes.setText(wallpaper.getFavorites());
        if (!wallpaper.isPay()){
            txtCountWallpaper.setVisibility(View.GONE);
        } else {
            Double price = wallpaper.get_Price();
            if (price != null) {
                txtCountWallpaper.setVisibility(View.VISIBLE);
                txtCountWallpaper.setText("$" + String.format( "%.2f", price));
            }
        }
        if(hasLikes(wallpaper.getObjectId()))
            likeYesVisible();
        else
            likeNoVisible();
        txtTagsList.setText(Helper.parseTags(wallpaper.getTags()));

        if(hasDownloads(wallpaper.getObjectId())){
            downloadActive();
        }else{
            downloadNoActive();
        }
    }

    private void downloadActive(){
        btnDownload.setVisibility(View.GONE);
        downloadLayout.setVisibility(View.VISIBLE);
        download.setVisibility(View.GONE);
        downloadActive.setVisibility(View.VISIBLE);
    }

    private void downloadNoActive(){
        downloadLayout.setVisibility(View.GONE);
        btnDownload.setVisibility(View.VISIBLE);
        download.setVisibility(View.VISIBLE);
        downloadActive.setVisibility(View.GONE);
    }

    private boolean hasDownloads(String objectId){
        if(downloads == null) return false;
        for(String loadsId : downloads){
            if(loadsId.equals(objectId))
                return true;
        }
        return false;
    }

    private void likeYesVisible(){
        likeYesView.setVisibility(View.VISIBLE);
        likeNoView.setVisibility(View.GONE);
    }

    private void likeNoVisible(){
        likeYesView.setVisibility(View.GONE);
        likeNoView.setVisibility(View.VISIBLE);
    }

    public interface OnBaseViewListener {
        //        boolean onListenerEmptyField(EditText editText);
//        boolean onListenerValidEmailAddress(EditText editText);
        void onListenerBackClick();
        void onListenerEnableFragment(int WALLPAPER_DETAILS, String nameCategory);
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        private ModelWallpaper wallpaper;

        public DownloadFileFromURL(ModelWallpaper wallpaper) {
            this.wallpaper = wallpaper;
        }

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
//            pDialog.showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
//            progressBar.setVisibility(View.VISIBLE);
            try {
                URL url = new URL(f_url[0]);

                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                int length = connection.getContentLength();
                InputStream is = (InputStream) url.getContent();
                byte[] imageData = new byte[length];
                int buffersize = (int) Math.ceil(length / (double) 100);
                int downloaded = 0;
                int read;
                while (downloaded < length) {
                    if (length < buffersize) {
                        read = is.read(imageData, downloaded, length);
                    } else if ((length - downloaded) <= buffersize) {
                        read = is.read(imageData, downloaded, length
                                - downloaded);
                    } else {
                        read = is.read(imageData, downloaded, buffersize);
                    }
                    downloaded += read;
//                    publishProgress((downloaded * 100) / length);
                }
//                File file;
                String name;
                if(wallpapers.get(pagePosition).getLive()){
                    String d = wallpapers.get(pagePosition).getObjectId();
//                    d = d.replace("-" , "");
                    name = d + GIF;
//                    file = new File(wallpapers.get(pagePosition).getObjectId() + GIF);
                }else {
                    String d = wallpapers.get(pagePosition).getObjectId();
//                    d = d.replace("-" , "");
                    name = d + JPG;
//                    name = wallpapers.get(pagePosition).getObjectId() + JPG;
//                    file = new File(wallpapers.get(pagePosition).getObjectId() + JPG);
                }
                FileOutputStream outputStream;
                try {
                    outputStream = getActivity().openFileOutput(name, Context.MODE_PRIVATE);
                    outputStream.write(imageData);
                    outputStream.close();
                    Helper.logOut("SAVE FILE", "COMPLETED");
                } catch (Exception e) {
                    e.printStackTrace();
                    errorLoad = true;
                    Helper.logOut("SAVE ERROR", "ERROR: " + e.getMessage());
                }
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
//            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
//            dismissDialog(progress_bar_type);
            progressBar.setVisibility(View.GONE);
            if(!errorLoad) {
                LessWallpaper.loadWallpaperDownloadsForChange(wallpapers.get(pagePosition).getObjectId());
                if (downloads == null) downloads = new ArrayList<>();
                downloads.add(wallpapers.get(pagePosition).getObjectId());
                PreferenceHelper.writeStringDataList(getActivity(), PreferenceHelper.DOWNLOADS_LIST_KEY, downloads);
                downloadActive();
                int downloads = Integer.parseInt(wallpaper.getDownloads());
                downloads ++;
                wallpaper.setDownloads(downloads);
                txtCountDownloads.setText(wallpaper.getDownloads());
            }
        }
    }
}

