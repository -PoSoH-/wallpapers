package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.gulievferdihotmail.wellpaperslife.Adapter.WallpaperList;
import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessWallpaper;
import com.gulievferdihotmail.wellpaperslife.Models.ModelCategories;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;
import com.gulievferdihotmail.wellpaperslife.R;

import java.util.ArrayList;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class WallpapersView extends Fragment {

    private OnWallpapersListener wellpaperListener;
    private GridView gridWallpapers;
    private int[] fakeResource;
    private WallpaperList adapterWallpaper;
    private String _LabelName = "New";
    private ArrayList<ModelWallpaper> wallpapersList = new ArrayList<>();

    public static WallpapersView create(){return new WallpapersView();}

    public static final String SEARCH_BROADCAST_RECEIVER = "com.wallpaper.SEARCH_BROADCAST_RECEIVER",
            SEARCH_STRING = "SEARCH_STRING";

    private BroadcastReceiver searchBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadWallpapers(intent.getStringExtra(SEARCH_STRING));
        }
    };

    public static void search(Context context, String searchString) {
        Intent intent = new Intent(SEARCH_BROADCAST_RECEIVER);
        intent.putExtra(SEARCH_STRING, searchString);
        context.sendBroadcast(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnWallpapersListener)
            wellpaperListener = (OnWallpapersListener) context;
        else
            throw new ClassCastException("Error! Please implements OnWallpapersListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        wellpaperListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_wallpapers, container, false);
        gridWallpapers = (GridView)view.findViewById(R.id.grid_wallpaper);
        adapterWallpaper = new WallpaperList(getActivity(), wallpapersList);
        loadWallpapers(null);
        return view;
    }

    private void loadWallpapers(String searchString) {
        LessWallpaper.loadWallpapers(searchString, null, null, new LessWallpaper.OnCallBackWallpaperListener() {
            @Override
            public void onSuccess(ArrayList<ModelWallpaper> wallpapers) {
                wallpapersList = wallpapers;
                adapterWallpaper.addWallpaperList(wallpapersList);
            }

            @Override
            public void onError() {
                ErrorMessage.show(getActivity()
                        , "Error load wallpapers"
                        , ErrorMessage.DIALOG_TYPE_ERROR);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getContext().registerReceiver(searchBroadcastReceiver, new IntentFilter(SEARCH_BROADCAST_RECEIVER));
        initialiseButtonClick();
//        createData();

        gridWallpapers.setAdapter(adapterWallpaper);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(searchBroadcastReceiver);
    }

    public void  startLoadCategory(ModelCategories category){
        LessWallpaper.loadWallpapers(null, category.getObjectId(), null, new LessWallpaper.OnCallBackWallpaperListener() {
            @Override
            public void onSuccess(ArrayList<ModelWallpaper> categories) {
                wallpapersList = categories;
                adapterWallpaper.addWallpaperList(wallpapersList);
            }

            @Override
            public void onError() {

            }
        });
    }

    public void startVisibilityCategory(ArrayList<String> listVallpapers){
        wallpapersList = new ArrayList<>();
        adapterWallpaper.addWallpaperList(wallpapersList);
        LessWallpaper.loadWallpapers(null, null, listVallpapers, new LessWallpaper.OnCallBackWallpaperListener() {
            @Override
            public void onSuccess(ArrayList<ModelWallpaper> categories) {
                wallpapersList = categories;
                adapterWallpaper.addWallpaperList(wallpapersList);
            }

            @Override
            public void onError() {

            }
        });
    }

    public void removeItemGrid(){
        adapterWallpaper.removeItems();
    }

    private void createData(){

        int mnog = 2;

        int[] drawables = new int[4];
//        drawables[0] = R.drawable.one;
//        drawables[1] = R.drawable.three;
//        drawables[2] = R.drawable.two;
//        drawables[3] = R.drawable.four;
//        for(int p=0, l=dataFile.length; p<l;p++) {
//            drawables[p] = getActivity().getPackageCodePath() + "/assets/static/" + dataFile[p];
//        }

        fakeResource = new int[drawables.length*mnog];
        int count = 0;
        for(int p=0, l=drawables.length*mnog; p<l;p++){
            fakeResource[p] = drawables[count];
            count++;
            if(count == drawables.length)
                count = 0;
        }
    }

    private void initialiseButtonClick(){
        gridWallpapers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (wellpaperListener != null) {
                    wellpaperListener.onListenerWallpaperSelectItem(wallpapersList, position);
                }
            }
        });
    }

    public String getLabelName(){
        return this._LabelName;
    }
    public void setLabelName(String newLabelName) {
        this._LabelName = newLabelName;
    }

    public interface OnWallpapersListener {
//        boolean onListenerEmptyField(EditText editText);
//        boolean onListenerValidEmailAddress(EditText editText);
        void onListenerWallpaperSelectItem(ArrayList<ModelWallpaper> wallpaper, int position);
    }
}

