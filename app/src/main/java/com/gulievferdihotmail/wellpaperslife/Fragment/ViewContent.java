package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;

import com.gulievferdihotmail.wellpaperslife.Adapter.WallpaperCustom;
import com.gulievferdihotmail.wellpaperslife.Models.ModelCategories;
import com.gulievferdihotmail.wellpaperslife.R;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class ViewContent extends Fragment {

    private OnViewPagerListener viewPagerListener;

    public static final int VIEW_WALLPAPERS = 1;
    public static final int VIEW_CATEGORIES = 0;

    private ViewPager viewPager;
    private WallpaperCustom adapterPage;
    private View btnCategories;
    private View btnWallpapers;

    public static ViewContent create(){return new ViewContent();}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnViewPagerListener)
            viewPagerListener = (OnViewPagerListener) context;
        else
            throw new ClassCastException("Error! Please implements OnViewPagerListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewPagerListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_view_pager, container, false);
        viewPager = (ViewPager)view.findViewById(R.id.view_pager_main_activity);
        btnCategories = view.findViewById(R.id.txt_list_categories_name);
        btnWallpapers = view.findViewById(R.id.txt_list_wallpapers_name);
        initialiseButtonClick();
        adapterPage = new WallpaperCustom(getFragmentManager());
        viewPager.setAdapter(adapterPage);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewPager.setCurrentItem(VIEW_WALLPAPERS);
        initialiseButtonClick();
        setMarkerLabel(viewPager.getCurrentItem());
    }

    private void setMarkerLabel(int countPager){
        if(viewPagerListener != null) {
            switch (countPager) {
                case VIEW_WALLPAPERS:
                    viewPagerListener.onListenerEnableFragment(VIEW_WALLPAPERS, ((WallpapersView)adapterPage.getItem(VIEW_WALLPAPERS)).getLabelName());
                    break;
                case VIEW_CATEGORIES:
                    viewPagerListener.onListenerEnableFragment(VIEW_CATEGORIES, ((CategoriesView)adapterPage.getItem(VIEW_CATEGORIES)).getLabelName());
                    break;
                default:
                    viewPagerListener.onListenerEnableFragment(-1, null);
            }
        }
    }

    public Fragment getActiveFragment(){
        if(adapterPage.getItem(viewPager.getCurrentItem()) instanceof WallpapersView)
            return adapterPage.getItem(viewPager.getCurrentItem());
        else if(adapterPage.getItem(viewPager.getCurrentItem()) instanceof CategoriesView)
            return adapterPage.getItem(viewPager.getCurrentItem());
        else
            return null;
    }

    private void initialiseButtonClick(){

        btnCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(VIEW_CATEGORIES);
            }
        });

        btnWallpapers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(VIEW_WALLPAPERS);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setMarkerLabel(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        btnSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Helper.logOut("LOGIN", "login button");
//                if(forgotListener != null){
//                    if(!forgotListener.onListenerEmptyField(editEmailAddress)) {
//                        ErrorMessage.show(getActivity()
//                                , getString(R.string.txt_error_email)
//                                , ErrorMessage.DIALOG_TYPE_ERROR);
//                    }else if(!forgotListener.onListenerValidEmailAddress(editEmailAddress)) {
//                        ErrorMessage.show(getActivity()
//                                , getString(R.string.txt_error_email_correctly)
//                                , ErrorMessage.DIALOG_TYPE_ERROR);
//                    }else{
//                            Helper.logOut("LOGIN", "LOGIN IS SUCCESS");
//                            forgotListener.onListenerStartNextScreen(Helper.TYPE_CHANGE_PASSWORD);
//                    }
//                }
//            }
//        });
    }

    public void selectWallpaperGridView(){
        viewPager.setCurrentItem(VIEW_WALLPAPERS);
    }

    public void enableWallpaperGrig(ModelCategories category){
        viewPager.setCurrentItem(VIEW_WALLPAPERS);
        ((WallpapersView)adapterPage.getItem(VIEW_WALLPAPERS))
                .setLabelName(category.getNameCategory());
        ((WallpapersView)adapterPage.getItem(VIEW_WALLPAPERS))
                .removeItemGrid();
    }

    public interface OnViewPagerListener {
//        boolean onListenerEmptyField(EditText editText);
//        boolean onListenerValidEmailAddress(EditText editText);
        void onListenerEnableFragment(int page, String nameCategory);
    }
}

