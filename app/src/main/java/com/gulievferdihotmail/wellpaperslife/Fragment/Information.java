package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gulievferdihotmail.wellpaperslife.R;

/**
 * Created by ADMIN on 15.03.2016.
 */
public class Information extends Fragment {

    public static Fragment create(){return new Information();}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_information, container, false);
        return view;
    }
}
