package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.R;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class Forgot extends Fragment {

    private OnForgotListener forgotListener;
    private TextView btnSend;
    private EditText editEmailAddress;

    public static Forgot create(){return new Forgot();}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnForgotListener)
            forgotListener = (OnForgotListener) context;
        else
            throw new ClassCastException("Error! Please implements OnForgotListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        forgotListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_forgot, container, false);
        btnSend = (Button)view.findViewById(R.id.btn_forgot);
        editEmailAddress = (EditText)view.findViewById(R.id.txt_forgot_email_address);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialiseButtonClick();
    }

    private void initialiseButtonClick(){
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.logOut("LOGIN", "login button");
                if(forgotListener != null){
                    if(!forgotListener.onListenerEmptyField(editEmailAddress)) {
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_email)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                    }else if(!forgotListener.onListenerValidEmailAddress(editEmailAddress)) {
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_email_correctly)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                    }else{
                            Helper.logOut("LOGIN", "LOGIN IS SUCCESS");
                            forgotListener.onListenerStartNextScreen(Helper.TYPE_CHANGE_PASSWORD);
                    }
                }
            }
        });
    }

    public interface OnForgotListener {
        boolean onListenerEmptyField(EditText editText);
        boolean onListenerValidEmailAddress(EditText editText);
        void onListenerStartNextScreen(String typeNextScreen);
    }
}

