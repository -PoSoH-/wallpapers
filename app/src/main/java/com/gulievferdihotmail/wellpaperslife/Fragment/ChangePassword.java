package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Activity.Main;
import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.R;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class ChangePassword extends Fragment {

    private OnChangePasswordListener loginListener;

    private TextView btnChange;

    private EditText editLastPassword;
    private EditText editNewPassword;
    private EditText editNewChackPassword;

    public static ChangePassword create(){return new ChangePassword();}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnChangePasswordListener)
            loginListener = (OnChangePasswordListener) context;
        else
            throw new ClassCastException("Error! Please implements OnChangePasswordListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        loginListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_change_password, container, false);
        btnChange = (Button)view.findViewById(R.id.btn_change_password);
        editLastPassword = (EditText)view.findViewById(R.id.txt_change_last_password);
        editNewPassword = (EditText)view.findViewById(R.id.txt_change_new_password);
        editNewChackPassword = (EditText)view.findViewById(R.id.txt_change_check_new_password);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialiseButtonClick();
    }

    private void initialiseButtonClick(){
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.logOut("LOGIN", "login button");
                if(loginListener != null){
                    if(!loginListener.onListenerEmptyField(editLastPassword)) {
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_short_password_login)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }
                    if(!loginListener.onListenerEmptyField(editNewPassword)) {
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_password)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                    }else if(loginListener.onListenerValidNumberPassword(editNewPassword)){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_short_password_login)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                    }else if(loginListener.onListenerEmptyField(editNewChackPassword)){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_password)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                    }else if(loginListener.onListenerValidNumberPassword(editNewChackPassword)){
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_short_password_check)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                    }else{
                        Main.show(getActivity());
                    }
                }
            }
        });
    }

    public interface OnChangePasswordListener {
        boolean onListenerEmptyField(EditText editText);
        boolean onListenerValidNumberPassword(EditText editText);
    }
}

