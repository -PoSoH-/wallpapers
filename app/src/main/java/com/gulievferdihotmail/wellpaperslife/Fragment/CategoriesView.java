package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gulievferdihotmail.wellpaperslife.Activity.Main;
import com.gulievferdihotmail.wellpaperslife.Adapter.CategoryList;
import com.gulievferdihotmail.wellpaperslife.Helpers.PreferenceHelper;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessCategories;
import com.gulievferdihotmail.wellpaperslife.Models.ModelCategories;
import com.gulievferdihotmail.wellpaperslife.Models.ModelWallpaper;
import com.gulievferdihotmail.wellpaperslife.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class CategoriesView extends Fragment {

    private OnCategoriesListener categoryListener;
    private ListView listCategories;
    private CategoryList adapterList;
    private String _LabelName = "All categories";
    private ArrayList<ModelCategories> categoriesList = new ArrayList<>();

    public static CategoriesView create(){return new CategoriesView();}

    private BroadcastReceiver buyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String boughtProductId = intent.getStringExtra(Main.BOUGHT_PRODUCT_ID);
            if (categoriesList != null) {
                for (ModelCategories modelCategory : categoriesList) {
                    if (modelCategory.getProductId() != null && modelCategory.getProductId().equals(boughtProductId)) {
                        PreferenceHelper.addPurchasedId(boughtProductId, getContext());
                        categoryListener.onListenerSelectedItem(modelCategory);
                        break;
                    }
                }
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(buyBroadcastReceiver, new IntentFilter(Main.BOUGHT_WALLPAPER_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(buyBroadcastReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnCategoriesListener)
            categoryListener = (OnCategoriesListener) context;
        else
            throw new ClassCastException("Error! Please implements OnCategoriesListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        forgotListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_categories, container, false);
        listCategories = (ListView)view.findViewById(R.id.list_categories);

        LessCategories.loadCategories(new LessCategories.OnCallBackCategoriesListener() {
            @Override
            public void onSuccess(ArrayList<ModelCategories> categories) {
                categoriesList = categories;
                adapterList.addCategories(categoriesList);
            }

            @Override
            public void onError() {

            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialiseButtonClick();
//        createContent();
        adapterList = new CategoryList(getActivity(), categoriesList);
        listCategories.setAdapter(adapterList);
        listCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(categoryListener != null) {
                    ModelCategories category = categoriesList.get(position);
                    if (category.isPay()) {
                        // Check if category bought
                        if (PreferenceHelper.getStringDataList(getContext(),
                                PreferenceHelper.PURCHASE_LIST_KEY).contains(category.getObjectId()))
                            categoryListener.onListenerSelectedItem(category);
                        else
                            ((Main) getActivity()).getIapHelper().checkIfBoughtOrBuy(category.getProductId());
                    } else categoryListener.onListenerSelectedItem(category);
                }
            }
        });
    }

//    private void createContent(){
//        String []data = new String[]{
//                "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
//                "a", "s", "d", "f", "g", "h", "j", "k", "l", "z",
//                "x", "c", "v", "b", "n", "m"};
//        String []coun = new String[]{"one", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
//        for(int i=0, l=25; i<25; i++){
//            StringBuilder info = new StringBuilder();
//            StringBuilder value = new StringBuilder();
//            for(int q=0, e=15; q<e; q++) {
//                info.append(data[new Random().nextInt(data.length - 1)]);
//            }
//            this.info.add(info.toString());
//            for(int q=0, e=4; q<e; q++) {
//                value.append(data[new Random().nextInt(coun.length - 1)]);
//            }
//            this.value.add(value.toString());
//        }
//    }

    private void initialiseButtonClick(){
        View btnSearshOn = getActivity().findViewById(R.id.toolbar_btn_search);
        btnSearshOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(categoryListener != null)
                    categoryListener.onListenerSearchView();
            }
        });
    }

    public String getLabelName(){
        return this._LabelName;
    }
    public void setLabelName(String newLabelName) {
        this._LabelName = newLabelName;
    }

    public interface OnCategoriesListener {
        void onListenerSearchView();
        void onListenerSelectedItem(ModelCategories category);
        void onCategoriesLoaded(List<ModelCategories> categories);
//        boolean onListenerEmptyField(EditText editText);
//        boolean onListenerValidEmailAddress(EditText editText);
//        void onListenerStartNextScreen(String typeNextScreen);
    }
}

