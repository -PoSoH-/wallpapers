package com.gulievferdihotmail.wellpaperslife.Fragment.Dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.R;
import com.gulievferdihotmail.wellpaperslife.Start;

public class ErrorMessage extends DialogFragment {

    public static final String DATA = "-DATA-MESSAGE-DIALOG-";
    public static final String TYPE_DIALOG = "-DATA-TYPE-DIALOG-";
    public static final int DIALOG_TYPE_ERROR = -1;
    public static final int DIALOG_TYPE_SUCCESS = -2;

    public static void show(FragmentActivity fragmentActivity, String message, int typeDialog) {
        ErrorMessage dialog = new ErrorMessage();
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        fragmentActivity.getIntent().putExtra(DATA, message);
        fragmentActivity.getIntent().putExtra(TYPE_DIALOG, typeDialog);
        ft.add(dialog, Dialog.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.d_error_message,
                null);
        Helper.logOut("ERROR MESSAGES", getActivity().getIntent().getIntExtra(TYPE_DIALOG, 0) + "");
        if(getActivity().getIntent().getIntExtra(TYPE_DIALOG, 0) == DIALOG_TYPE_ERROR){
            TextView text = (TextView) view.findViewById(R.id.tv_error_message_text);
            text.setVisibility(View.VISIBLE);
            text.setText(getActivity().getIntent().getStringExtra(DATA));
        }else{
            TextView text = (TextView) view.findViewById(R.id.tv_success_message_text);
            text.setVisibility(View.VISIBLE);
            text.setText(getActivity().getIntent().getStringExtra(DATA));
        }
        view.findViewById(R.id.tv_error_message_btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        return dialog;
    }

}
