package com.gulievferdihotmail.wellpaperslife.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gulievferdihotmail.wellpaperslife.Fragment.Dialog.ErrorMessage;
import com.gulievferdihotmail.wellpaperslife.Helpers.Helper;
import com.gulievferdihotmail.wellpaperslife.Loaders.LessUsers;
import com.gulievferdihotmail.wellpaperslife.R;

import java.util.HashMap;

/**
 * Created by Sergiy Polishuk on 05.03.2016.
 */
public class Login extends Fragment {

    private OnLoginListener loginListener;

    private TextView txtForgotPassword;
    private TextView txtCreateAnAccount;
    private TextView btnLogIn;

    private EditText editEmailAddress;
    private EditText editPassword;

    public static Login create(){return new Login();}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnLoginListener)
            loginListener = (OnLoginListener) context;
        else
            throw new ClassCastException("Error! Please implements OnLoginListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        loginListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_login, container, false);
        txtCreateAnAccount = (TextView)view.findViewById(R.id.txt_login_btn_create);
        txtForgotPassword = (TextView)view.findViewById(R.id.txt_login_btn_forgot);
        btnLogIn = (Button)view.findViewById(R.id.btn_login);
        editEmailAddress = (EditText)view.findViewById(R.id.txt_login_email_address);
        editPassword = (EditText)view.findViewById(R.id.txt_login_password);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialiseButtonClick();
    }

    private void initialiseButtonClick(){
        txtCreateAnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.logOut("LOGIN", "create button");
                if(loginListener!=null)
                    loginListener.onListenerStartNextScreen(Helper.TYPE_REGISTRATION);
            }
        });
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.logOut("LOGIN", "forgot button");
                if(loginListener!=null)
                    loginListener.onListenerStartNextScreen(Helper.TYPE_FORGOT_PASSWORD);
            }
        });
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.logOut("LOGIN", "login button");
                if(loginListener != null){
                    boolean emailEmpty = false;
                    boolean passwordEmpty = false;
                    emailEmpty = loginListener.onListenerEmptyField(editEmailAddress);
                    passwordEmpty = loginListener.onListenerEmptyField(editPassword);
//                    if(!emailEmpty && !passwordEmpty) {
//                        ErrorMessage.show(getActivity()
//                                , getString(R.string.txt_error_email)
//                                        + "\n"
//                                        + getString(R.string.txt_error_password)
//                                , ErrorMessage.DIALOG_TYPE_ERROR);
//                    }

                    if(!emailEmpty) {
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_email)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }

                    if(!passwordEmpty) {
                        ErrorMessage.show(getActivity()
                                , getString(R.string.txt_error_password)
                                , ErrorMessage.DIALOG_TYPE_ERROR);
                        return;
                    }

                    if(emailEmpty && passwordEmpty) {
                        boolean emailCorectly = loginListener.onListenerValidEmailAddress(editEmailAddress);
                        boolean passwordCorectly = loginListener.onListenerValidNumberPassword(editPassword);
//                        if(!emailCorectly & !passwordCorectly){
//                            ErrorMessage.show(getActivity()
//                                    , getString(R.string.txt_error_email_correctly)
//                                            + "\n"
//                                            + getString(R.string.txt_error_short_password_login)
//                                    , ErrorMessage.DIALOG_TYPE_ERROR);
//                        }
                        if(!emailCorectly){
                            ErrorMessage.show(getActivity()
                                    , getString(R.string.txt_error_email_correctly)
                                    , ErrorMessage.DIALOG_TYPE_ERROR);
                            return;
                        }
                        if(!passwordCorectly){
                            ErrorMessage.show(getActivity()
                                    , getString(R.string.txt_error_short_password_login)
                                    , ErrorMessage.DIALOG_TYPE_ERROR);
                            return;
                        }
                        if(emailCorectly & passwordCorectly){
                            Helper.logOut("LOGIN", "LOGIN IS SUCCESS");
                            HashMap<String, String> infoUser = new HashMap<>();
                            infoUser.put(LessUsers.TU_EMAIL, editEmailAddress.getText().toString());
                            infoUser.put(LessUsers.TU_PASSWORD, editPassword.getText().toString());
                            LessUsers.loginUser(infoUser, new LessUsers.OnResponseAccountListener() {
                                @Override
                                public void onSuccess() {
                                    loginListener.onListenerStartNextScreen(Helper.TYPE_MAIN);
                                }

                                @Override
                                public void onError() {
                                    ErrorMessage.show(getActivity()
                                            , "this user is not exist!.."
                                            , ErrorMessage.DIALOG_TYPE_ERROR);
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    public interface OnLoginListener {
        boolean onListenerEmptyField(EditText editText);
        boolean onListenerValidEmailAddress(EditText editText);
        boolean onListenerValidNumberPassword(EditText editText);
        void onListenerStartNextScreen(String typeNextScreen);
    }
}

